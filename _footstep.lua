_footstep = {

	instances = {},
	
	leftImg01 = love.graphics.newImage("gfx/footstepLeft01.png"),
	rightImg01 = love.graphics.newImage("gfx/footstepRight01.png"),
	leftImg02 = love.graphics.newImage("gfx/footstepLeft02.png"),
	rightImg02 = love.graphics.newImage("gfx/footstepRight02.png"),

	drawAll = function(c_footstep)
		for i,v in ipairs(c_footstep.instances) do
			v:draw(c_footstep)
		end
	end,
	
	updateAll = function(c_footstep, dt)
		for i,v in ipairs(c_footstep.instances) do
			v:update(dt)
		end
		c_footstep:remove()
	end,
	
	remove = function(c_footstep)
		for i=#c_footstep.instances, 1, -1 do
			if c_footstep.instances[i].removeable == true then
				table.remove(c_footstep.instances, i)
			end		
		end
	end,
	
	clear = function()
		for i=#_footstep.instances, 1, -1 do
			_footstep.instances[i].removeable = true
		end
		_footstep:remove()
	end,
	
	new = function(c_footstep, parameters)
		local ret = {}
		local mt = {
			__tostring = function(self)
				return "<_line instance>"
			end,
			}
		setmetatable(ret, mt)
		local p = parameters or {}
		
		ret.x = p.x or 0.5 * love.graphics.getWidth()
		ret.y = p.y or 0.5 * love.graphics.getHeight()
		ret.r = p.r or math.rad(love.math.random(0, 360))
		ret.lifetime = p.lifetime or 10
		local cr, cg, cb = _map.getCurrentTileRGB(p.x, p.y)
		ret.cr = p.cr or cr
		ret.cg = p.cg or cg
		ret.cb = p.cb or cb
		ret.ca = 255
		ret.left = p.left
		ret.right = p.right
		ret.leftImg = p.leftImg or c_footstep.leftImg01
		ret.rightImg = p.rightImg or c_footstep.rightImg01
		
		ret.removeable = false
		ret.degradation = p.degradation or 255/ret.lifetime

		ret.draw = function(i_footstep, c_footstep)
			love.graphics.setColor(i_footstep.cr, i_footstep.cg, i_footstep.cb, i_footstep.ca)
			if i_footstep.left then
				love.graphics.draw(i_footstep.leftImg, i_footstep.x, i_footstep.y, i_footstep.r + math.rad(90), 1, 1, 0.75 * i_footstep.leftImg:getWidth(), 0.5 * i_footstep.leftImg:getHeight())
			end
			if i_footstep.right then
				love.graphics.draw(i_footstep.rightImg, i_footstep.x, i_footstep.y, i_footstep.r + math.rad(90), 1, 1, 0.25 * i_footstep.rightImg:getWidth(), 0.5 * i_footstep.rightImg:getHeight())
			end
		end

		ret.update = function(i_footstep, dt)
			i_footstep.ca = i_footstep.ca - dt*i_footstep.degradation
			if i_footstep.ca < 0 then
				i_footstep.ca = 0
				i_footstep.removeable = true
			end
		end

		--> Add line to instances!
		table.insert(c_footstep.instances, ret)
		return ret
	end
}