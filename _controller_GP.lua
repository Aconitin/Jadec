return function(c_controller, keyTable, gamepad) --> Creates a new controller for a gamepad!

	local ret = {} --> Controller Instance!
	
	--> Set metatable!
	local mt = {
		__tostring = function(self)
			return "<Instance of class: _controller_GP. Can handle specific input keys.>"
		end
	}
	setmetatable(ret, mt)
	
	ret.callbacks = {} --> Holds (key, callback) pairs!
	ret.gamepad = gamepad --> Holds the gamepad this controller is using!
	ret.type = "gp"
	ret.active = true
	
	--> Check if the keyTable is valid!
	if not keyTable then
	_controller:debugPrint("KeyTable is not valid. Creating a default one!")
	end
	ret.keyTable = keyTable or {upDown = _G.keys.gamepadUpDown, --> Integers are the axis numbers (Of the axis that will be used for that key)!
								leftRight = _G.keys.gamepadLeftRight,
								sneak = _G.keys.gamepadSneak,
								running = _G.keys.gamepadRun,
								throwing = _G.keys.gamepadThrow, --> This is not a callback. When this is active, players are about to throw and can't move!
								throw = tostring(ret.gamepad:getID()) .. "gu" .. _G.keys.gamepadThrow, --> This callback actually triggers the throw!
								throwIdle = tostring(ret.gamepad:getID()) .. "gd" .. _G.keys.gamepadThrow, --> For idle!
								clap = tostring(ret.gamepad:getID()) .. "gu" .. _G.keys.gamepadClap,
								join = tostring(ret.gamepad:getID()) .. "gd" .. _G.keys.gamepadJoin, --> key for joining the game. Also a callback key!
								} --> Default keyTable. The controller instance will listen to these keys and return movementMatrices accordingly. Axis are Integers!
								
	--> Add getMovementMatrix function!
	ret.getMovementMatrix = function(i_controller)
		local ret = {upDown = 0, leftRight = 0}
		local upDown = i_controller.gamepad:getAxis(i_controller.keyTable.upDown)
		local leftRight = i_controller.gamepad:getAxis(i_controller.keyTable.leftRight)
		local threshold = 0.2 --> Prevents players from floating around if the gamepad axis is not centered properly!
		if upDown > threshold then
			ret.upDown = 1
		elseif upDown < -threshold then
			ret.upDown = -1
		end
		if leftRight > threshold then
			ret.leftRight = 1
		elseif leftRight < -threshold then
			ret.leftRight = -1
		end
		return ret
	end
	
	--> Registers a callback for this controller. Returns callback ID!
	ret.registerCallback = function(i_controller, callbackFunction, triggerKey, parameters, callbackID) --> Parameters is a table!
	_controller:debugPrint("Registered callback. Key = " .. triggerKey .. ", Name = " .. callbackID)
		i_controller.callbacks[callbackID] = {triggerKey = triggerKey, callbackFunction = callbackFunction, parameters = parameters}
	end
	
	--> Unregisters a callback for this controller!
	ret.unregisterCallback = function(i_controller, callbackID)
		_controller:debugPrint("Unregistered callback " .. callbackID)
		if i_controller.callbacks[callbackID] then
			i_controller.callbacks[callbackID] = nil
		end
	end
	
	--> Returns movementMatrix!
	ret.getFullUpdate = function(i_controller)
		local m = i_controller:getMovementMatrix()
		m.isSneaking = (i_controller.gamepad:getAxis(i_controller.keyTable.sneak) > 0)
		m.isThrowing = i_controller.gamepad:isGamepadDown(i_controller.keyTable.throwing)
		m.isRunning = (i_controller.gamepad:getAxis(i_controller.keyTable.running) > 0)
		return m
	end

	_controller:debugPrint("Created controller (GP)!")
	
	--> Push instance back to main class!
	table.insert(c_controller.instances, ret)
	return c_controller:getController(c_controller:getCount())
end