_whisp = {

	--[[ Info
	
		Uses: _collider, _afx

		drawAll()
		draw(i_whisp)
		updateAll(dt)
		update(i_whisp, dt)
		collide(i_whisp, i_other)
		setTarget(i_whisp, x, y, age)
		remove(i_whisp, ID)
		clear()
		create(parameters)
		
	--]]

	--> Holds all whisp instances!
	instances = {},

	--> Draws all class instances!
	drawAll = function()
		for i,v in ipairs(_whisp.instances) do
			_whisp.draw(v)
		end
	end,
	
	--> Draws a single instance!
	draw = function(i_whisp)
		love.graphics.setColor(i_whisp.r, i_whisp.g, i_whisp.b, i_whisp.a)
		love.graphics.draw(i_whisp.particleSystem, 0, 0)
		if __drawColliders then
			love.graphics.setColor(255, 255, 0, 150)
			i_whisp.collider:draw()
			love.graphics.setColor(255, 255, 255, 255)
			love.graphics.rectangle("fill", i_whisp.x-2.5, i_whisp.y-2.5, 5, 5)
		end
	end,
	
	--> Updates all class instances!
	updateAll = function(dt)
		for i=#_whisp.instances, 1, -1 do
			local v = _whisp.instances[i]
			if v.removeable then
				_whisp.remove(v, i)
			else
				_whisp.update(v, dt)
			end	
		end
	end,
	
	--> Updates one whisp instance!
	update = function(i_whisp, dt)
	
		i_whisp.particleSystem:update(dt)
	
		if i_whisp.target then
		
			i_whisp.a = i_whisp.a + dt * 255 * 4
			if i_whisp.a > 255 then
				i_whisp.a = 255
			end
		
			--> Move whisp to target position!
			local angleInDegrees = math.rad(math.atan2(i_whisp.target.y - i_whisp.y, i_whisp.target.x - i_whisp.x) * 180 / math.pi)
			local dx = i_whisp.s*dt*math.cos(angleInDegrees)
			local dy = i_whisp.s*dt*math.sin(angleInDegrees)
			i_whisp.x = i_whisp.x + dx
			i_whisp.y = i_whisp.y + dy
			i_whisp.collider:moveTo(i_whisp.x, i_whisp.y)
			i_whisp.particleSystem:setPosition(i_whisp.x, i_whisp.y)

			--> If there (or nearly there), disable target!
			if math.abs(i_whisp.target.x - i_whisp.x) < math.abs(dx) and math.abs(i_whisp.target.y - i_whisp.y) < math.abs(dy) then
				i_whisp.target = nil
				i_whisp.targetAge = nil
			end
			
			--> Sound!
			if not i_whisp.sound then
				i_whisp.sound = _afx:play("enemyWhispLoop.ogg", {loop = true, seek = love.math.random(0, 123), fade = -1.5, x = i_whisp.x, y = i_whisp.y, attenuation = {min = 0, max = 1000}})
			else
				i_whisp.sound.source:setPosition(i_whisp.x, i_whisp.y)
			end
		else
			if i_whisp.sound and not i_whisp.sound.fade then
				_afx:stop(i_whisp.sound, {fade = 3})
				i_whisp.sound = nil
			end
			i_whisp.a = i_whisp.a - dt * 255 * 0.5
			if i_whisp.a < 0 then
				i_whisp.a = 0
			end
		end
	end,
	
	--> Handles collision!
	collide = function(i_whisp, i_other)
		if i_other.className and (i_other.className == "_line") then
			_whisp.setTarget(i_whisp, i_other.origin.x or i_other.pos[1], i_other.origin.y or i_other.pos[2], i_other.age)
		end
	end,
	
	--> Sets a new position to move to!
	setTarget = function(i_whisp, x, y, age)
		if not i_whisp.target or i_whisp.targetAge > age then
			i_whisp.target = {x = x, y = y}
			i_whisp.targetAge = age
		end
	end,
	
	--> Removes a single whisp!
	remove = function(i_whisp, ID)
		i_whisp.removeable = true
		_collider.HC:remove(i_whisp.collider)
		i_whisp.collider = nil
		if i_whisp.sound then
			_afx:stop(i_whisp.sound, {fade = 0.5})
		end
		table.remove(_whisp.instances, ID)
	end,
	
	--> Removes all whisps!
	clear = function()
		for i=#_whisp.instances,1,-1 do
			_whisp.remove(_whisp.instances[i], i)
		end
	end,
		
	--> Creates and returns a new instance!
	create = function(parameters)
		local ret = {}
		local p = parameters or {}
		
		ret.className = "_whisp"
		ret.type = "enemy"
		
		ret.r = parameters.r or _G.colours.danger.r or 255
		ret.g = parameters.g or _G.colours.danger.g or 0
		ret.b = parameters.b or _G.colours.danger.b or 0
		ret.a = parameters.a or 0

		ret.x = p.x or 300
		ret.y = p.y or 300
		ret.s = p.s or love.math.random(100, 150) --> Movement speed of instance!
		
		ret.target = nil
		ret.targetAge = nil
		ret.collider = _collider.HC:addCircle(ret.x, ret.y, 30)
		_collider.HC:addToGroup(ret.type, ret.collider)
		ret.collider.backLink = ret
		ret.sound = nil
				
		ret.pImgX = 6
		ret.pImgY = 6
		ret.pAmount = 128
		ret.pImg = love.graphics.newImage((function() --> Particle images!
			local imageData = love.image.newImageData(ret.pImgX, ret.pImgY)
			for x=0, ret.pImgX-1, 1 do
				for y=0, ret.pImgY-1, 1 do
					imageData:setPixel(x, y, 255, 255, 255, 255)
				end
			end
			return imageData
		end)())		
				
		ret.particleSystem = love.graphics.newParticleSystem(ret.pImg, ret.pAmount)
		ret.particleSystem:setEmissionRate(128)
		ret.particleSystem:setEmitterLifetime(60*60*24*365)
		ret.particleSystem:setParticleLifetime(1)
		ret.particleSystem:setLinearAcceleration(-5, -5, 5, 5)
		ret.particleSystem:setRadialAcceleration(-5, -5)
		ret.particleSystem:setTangentialAcceleration(-5, 5)
		ret.particleSystem:setSpinVariation(0, 1)
		ret.particleSystem:setColors(255, 255, 255, 255, 255, 255, 255, 0) --> Fade to black!
		ret.particleSystem:setAreaSpread("normal", 12, 12)
		ret.particleSystem:setPosition(ret.x, ret.y)
				
		--> Finish creation, insert to instances and return!
		table.insert(_whisp.instances, ret)
		return ret
	end
}