_stone = {

	instances = {},
	
	updateAll = function(dt)
		for i=#_stone.instances, 1, -1 do
			_stone.update(_stone.instances[i], dt)
			if _stone.instances[i].removeable then
				table.remove(_stone.instances, i)
			end
		end
	end,
	
	drawAll = function()
		for i,v in pairs(_stone.instances) do
			_stone.draw(v)
		end
	end,
	
	update = function(i_stone, dt)
		i_stone.x = i_stone.x + i_stone.s * math.cos(i_stone.r) * dt
		i_stone.y = i_stone.y + i_stone.s * math.sin(i_stone.r) * dt
		i_stone.travelled = i_stone.travelled + dt
		if _map.isBlocked(i_stone.x, i_stone.y) or i_stone.travelled >= i_stone.maxRange then
			if not i_stone.removeable then
				local x1 = i_stone.x - i_stone.s * math.cos(i_stone.r) * dt
				local y1 = i_stone.y - i_stone.s * math.sin(i_stone.r) * dt
				local x2 = i_stone.x - 2 * i_stone.s * math.cos(i_stone.r) * dt
				local y2 = i_stone.y - 2 * i_stone.s * math.sin(i_stone.r) * dt
				local x3 = i_stone.x - 3 * i_stone.s * math.cos(i_stone.r) * dt
				local y3 = i_stone.y - 3 * i_stone.s * math.sin(i_stone.r) * dt
				_stone.clack(x1, y1, x2, y2, x3, y3)
			end
			i_stone.removeable = true
		end
	end,
	
	draw = function(i_stone)
		if __drawColliders then
			love.graphics.setColor(255, 255, 255, 255)
			love.graphics.points(i_stone.x, i_stone.y)
		end
	end,
	
	create = function(x, y, r)
		local ret = {}
		ret.removeable = false
		ret.x = x or 0
		ret.y = y or 0
		ret.r = r or math.rad(0)
		ret.s = 750
		ret.maxRange = 1 --> In seconds!
		ret.travelled = 0
		_afx:play("stoneThrow.ogg")
	
		--> Add stone to instances!
		table.insert(_stone.instances, ret)
		return ret
	end,
	
	clack = function(x1, y1, x2, y2, x3, y3)
		_afx:play("stoneClack.ogg", {x = x1, y = y1})
		_line.createBunch("stonePrimary", {x = x1, y = y1})
		_line.createBunch("stoneSecondary", {x = x2, y = y2})
		_line.createBunch("stoneTertiary", {x = x3, y = y3})
	end,
}