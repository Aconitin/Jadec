_collider = {

	hardonCollider = nil, --> Gets required in main.lua!
	HC = nil,

	init = function()
		_collider.HC = _collider.hardonCollider.new(100, _collider.onCollide, _collider.stopCollide)
	end,
	
	onCollide = function(dt, s1, s2, dx, dy)
		if s1.backLink.className and _G[s1.backLink.className].collide then
			_G[s1.backLink.className].collide(s1.backLink, s2.backLink)
		end
		if s2.backLink.className and _G[s2.backLink.className].collide then
			_G[s2.backLink.className].collide(s2.backLink, s1.backLink)
		end
	end,

	stopCollide = function(dt, s1, s2, dx, dy)
	end,
}
