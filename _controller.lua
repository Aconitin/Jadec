_controller = { --> Alias: _c

	--> DebugPrint!
	debugPrint = function(c_controller, txt)
		--print(txt)
	end,

	instances = {},

	getCount = function(c_controller)
		return #c_controller.instances
	end,
	
	getController = function(c_controller, index)
		return c_controller.instances[index]
	end,
	
	getAllControllers = function(c_controller)
		return c_controller.instances
	end,
	
	--> If a callback has been registered and triggered, it gets called via this function!
	processCallback = function(c_controller, prefix, key, position, joystick)
		key = prefix .. key
		_controller:debugPrint("Processing callback with identity " .. key)
		for i,v in pairs(c_controller.instances) do
			if v.active then
				for k,m in pairs(v.callbacks) do
					local triggerKey = m.triggerKey
					local callbackFunction = m.callbackFunction
					local parameters = m.parameters
					if triggerKey == key or triggerKey == "all" then --> Callback is valid and has to be processed!
						_controller:debugPrint("Callback is valid: " .. tostring(callbackFunction) .. ". Processing now.")
						callbackFunction(unpack(parameters)) --> Call appropriate callback that has been registered earlier!
					end
				end
			end
		end
	end,
			
	--> Callback functions. These are the callbacks that get triggered, so every callback in the game is processed via the _controller class!
		--[[Prefixes for the processCallback function are as follows:
				k for keyboard or m for mouse or g for gamepad
				..
				u for up (release) or d for down (press)
		--]]
	keypressed = function(c_controller, key, isrepeat)
		c_controller:processCallback("kd", key)
	end,

	mousepressed = function(c_controller, x, y, button)
		c_controller:processCallback("md", button, {x = x, y = y})
	end,

	mousereleased = function(c_controller, x, y, button)
		c_controller:processCallback("mu", button, {x = x, y = y})
	end,

	keyreleased = function(c_controller, key)
		c_controller:processCallback("ku", key)
	end,
	
	gamepadpressed = function(c_controller, joystick, button)
		local id, instanceid = joystick:getID()
		local prefix = tostring(id) .. "gd"
		c_controller:processCallback(prefix, button, nil, joystick)
	end,
	
	gamepadreleased = function(c_controller, joystick, button)
		local id, instanceid = joystick:getID()
		local prefix = tostring(id) .. "gu"
		c_controller:processCallback(prefix, button, nil, joystick)
	end,
	
	newMK = require("_controller_MK"),
	newGP = require("_controller_GP"),
	
	searchForAllValidGamepads = function(c_controller)
		local j = love.joystick.getJoysticks()
		local valid = {}
		for i, joystick in ipairs(j) do
			if joystick:isGamepad() then
				table.insert(valid, joystick)
			end
		end
		return valid
	end,
	
	--> For game startup: Takes every gamepad + mouse & keyboard and sets up a controller for them. Mouse and Keyboard always has ID 1, rest follows!
	setupControllersForAllInputDevices = function(c_controller)
		c_controller:newMK()
		local gp = c_controller:searchForAllValidGamepads()
		_controller:debugPrint("Registered " .. #gp .. " distinct gamepads!")
		for i,v in ipairs(gp) do
			c_controller:newGP(nil, v)
		end
	end,

}