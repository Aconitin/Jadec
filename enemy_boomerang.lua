_boomerang = {
	
	--[[ Info
	
		Uses: _collider, _afx, _expandingCircle
		Managed by _boomerangTrap class!
		
		draw(i_boomerangTrap)
		update(i_boomerangTrap, dt)
		collide(i_boomerang, i_other)
		die(i_boomerangTrap)
		create(parameters)
	
	--]]
	
	--> Draws one boomerang!
	draw = function(i_sp)
		love.graphics.setColor(i_sp.r, i_sp.g, i_sp.b, i_sp.a)
		love.graphics.draw(i_sp.particleSystem, i_sp.x, i_sp.y, i_sp.o)

		--> For debug draws only!
		if __drawColliders then
			love.graphics.setColor(255, 255, 255, 255)
			love.graphics.setPointSize(10)
			love.graphics.points(i_sp.x, i_sp.y)
			if i_sp.collider then
				i_sp.collider:draw()
			end
		end
	end,
	
	--> Updates one boomerang!
	update = function(i_sp, dt)

		--> First update position and orientation!
		if not i_sp.deathTriggered then
			i_sp.o = math.rad(math.deg(i_sp.o) + dt * i_sp.so)
			i_sp.currentCircleAngle = i_sp.currentCircleAngle - math.rad(dt * 120)
		end

		if math.deg(i_sp.currentCircleAngle) <= -360 then
			i_sp.currentCircleAngle = math.rad(-360)
			if not i_sp.deathTriggered then
				_boomerang.die(i_sp)
			end
		end
		i_sp.x = i_sp.boomerangCenterX + math.cos(i_sp.currentCircleAngle + i_sp.to + math.rad(180)) * i_sp.boomerangRadius
		i_sp.y = i_sp.boomerangCenterY + math.sin(i_sp.currentCircleAngle + i_sp.to + math.rad(180)) * i_sp.boomerangRadius

		--> Then sound!
		if i_sp.sound then
			i_sp.sound.source:setPosition(i_sp.x, i_sp.y)
		end
		--> Then the particle system!
		i_sp.particleSystem:update(dt)
		if i_sp.particleSystem:getCount() == 0 then
			i_sp.removeable = true
		end

		--> And collider!
		if i_sp.collider then
			i_sp.collider:moveTo(i_sp.x, i_sp.y)
		end
	end,
	
	--> Handles collision of an instance!
	collide = function(i_boomerang, i_other)
		if i_other.className then
			if (i_other.className == "_player") then
				_boomerang.die(i_boomerang)
			end
		end
	end,
	
	--> Triggered when the boomerang returns to thrower!
	die = function(i_sp)
		i_sp.deathTriggered = true
		i_sp.particleSystem = love.graphics.newParticleSystem(i_sp.pImg, i_sp.pAmount)
		i_sp.particleSystem:setParticleLifetime(i_sp.pLifetimeMin, i_sp.pLifetimeMax)
		i_sp.particleSystem:setLinearAcceleration(0, 0)
		i_sp.particleSystem:setAreaSpread("uniform", i_sp.radius, i_sp.width)
		i_sp.particleSystem:setRadialAcceleration(-30)
		i_sp.particleSystem:setTangentialAcceleration(15, 15)
		i_sp.particleSystem:setColors(i_sp.r, i_sp.g, i_sp.b, i_sp.a, i_sp.r, i_sp.g, i_sp.b, 0) --> Fade to black!
		i_sp.particleSystem:emit(i_sp.pAmount)
		_collider.HC:remove(i_sp.collider)
		i_sp.collider = nil
		i_sp.collider = _collider.HC:addCircle(i_sp.x, i_sp.y, i_sp.radius/3)
		_collider.HC:addToGroup(i_sp.type, i_sp.collider)
		i_sp.collider.backLink = i_sp
		_afx:stop(i_sp.sound, {fade = 0.5})
		_afx:play("enemyBoomerangFinish.ogg", {x = i_sp.x, y = i_sp.y})
		_expandingCircle.new({x = i_sp.x,
							  y = i_sp.y,
							  r = _G.colours.danger.r,
							  g = _G.colours.danger.g,
							  b = _G.colours.danger.b,
							  a = 125,
							  radius = 0,
							  expandRate = 35,
							  lifetime = 1,
							  width = 4,
							  segments = 125,
							  degradation = 250,
							  rotation = math.rad(0)})
	end,

	--> Creates and returns a new instance!
	create = function(parameters)
	
		local ret = {} --> New _boomerang!
		
		--> General data!
		ret.className = "_boomerang" --> className.collide() is used for collisions!
		ret.type = "enemy" --> Used for e.g. collision groups!
		
		--> Set variables!
		local p = parameters
		p.color = p.color or {}
		ret.x = p.x or 0.5 * love.graphics.getWidth()
		ret.y = p.y or 0.5 * love.graphics.getHeight()
		ret.o = p.o or math.rad(0) --> Orientation!
		ret.to = p.to or math.rad(0) --> Travelorientation, direction to travel in!
		ret.s = p.s or 300 --> Speed!
		ret.so = p.so or 720 --> Rotation speed in degree per second!
		ret.radius = p.radius or 20 --> Radius/length of the line!
		ret.width = p.width or 5
		ret.r = p.color[1] or _G.colours.danger.r or 255
		ret.g = p.color[2] or _G.colours.danger.g or 35
		ret.b = p.color[3] or _G.colours.danger.b or 35
		ret.a = p.color[4] or 255
		ret.originX = p.originX or 0
		ret.originY = p.originY or 0
		
		ret.pImgX = 3 --> Particle System img size x!
		ret.pImgY = 3 --> Particle System img size y!
		ret.pAmount = 128 --> Particle amount (max)!
		ret.pAcceleration = 50
		ret.pLifetimeMin = 0.75
		ret.pLifetimeMax = 1.25
		ret.sound = _afx:play("enemyBoomerangLoop.ogg", {loop = true, x = ret.x, y = ret.y})

		ret.boomerangRadius = 0.5 * (math.sqrt((ret.x - ret.originX)^2 + (ret.y - ret.originY)^2))
		ret.boomerangCenterX = ret.x + math.cos(ret.to) * ret.boomerangRadius
		ret.boomerangCenterY = ret.y + math.sin(ret.to) * ret.boomerangRadius
		ret.currentCircleAngle = math.rad(0)

		--> Constants!
		ret.deathTriggered = false
		ret.pImg = love.graphics.newImage((function() --> Particle images!
			local imageData = love.image.newImageData(ret.pImgX, ret.pImgY)
			for x=0, ret.pImgX-1, 1 do
				for y=0, ret.pImgY-1, 1 do
					imageData:setPixel(x, y, ret.r, ret.g, ret.b, ret.a)
				end
			end
			return imageData
		end)())
		ret.particleSystem = love.graphics.newParticleSystem(ret.pImg, ret.pAmount)
		ret.particleSystem:setParticleLifetime(99, 99)
		ret.particleSystem:setAreaSpread("uniform", ret.radius, ret.width)
		ret.particleSystem:setColors(ret.r, ret.g, ret.b, ret.a, ret.r, ret.g, ret.b, ret.a) --> Fade to black!
		ret.particleSystem:emit(ret.pAmount)
		ret.collider = _collider.HC:addCircle(ret.x, ret.y, ret.radius)
		_collider.HC:addToGroup(ret.type, ret.collider)
		ret.collider.backLink = ret

		--> Add to instances!
		return ret
	end

}