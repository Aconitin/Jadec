_boomerangTrap = {

	--[[ Info
	
		Uses: _collider, _afx, _boomerang, _expandingCircle
		Manages _boomerang class!
		
		drawAll()
		draw(i_boomerangTrap)
		updateAll(dt)
		update(i_boomerangTrap, dt)
		remove(i_boomerangTrap, ID)
		clear()
		collide(i_boomerangTrap, i_other)
		throw(i_boomerangTrap, x, y)
		create(parameters)
	
	--]]

	--> Holds all boomerangTrap instances!
	instances = {},
	
	--> Draws all class instances!
	drawAll = function()
		for i,v in pairs(_boomerangTrap.instances) do
			_boomerangTrap.draw(v)
		end
	end,
	
	--> Draws a single instance!
	draw = function(i_boomerangTrap)
		if i_boomerangTrap.boomerang then
			_boomerang.draw(i_boomerangTrap.boomerang)
		end
		if __drawColliders then
			love.graphics.setColor(255, 255, 255, 150)
			i_boomerangTrap.collider:draw()
			love.graphics.setColor(255, 255, 255, 255)
			love.graphics.rectangle("fill", i_boomerangTrap.x-2.5, i_boomerangTrap.y-2.5, 5, 5)
		end
	end,
	
	--> Updates all class instances!
	updateAll = function(dt)
		for i=#_boomerangTrap.instances, 1, -1 do
			local v = _boomerangTrap.instances[i]
			if v.removeable then
				_boomerangTrap.remove(v, i) --> Remove trap if removeable!
			else
				_boomerangTrap.update(v, dt) --> Update given trap if not removeable!
			end
		end
	end,
	
	--> Update a trap instance!
	update = function(i_boomerangTrap, dt)
		if i_boomerangTrap.boomerang then
			if i_boomerangTrap.boomerang.removeable then
				_boomerangTrap.removeBoomerang(i_boomerangTrap)
			else
				_boomerang.update(i_boomerangTrap.boomerang, dt)
			end
		end
	end,
	
	--> Remove a trap instance!
	remove = function(i_boomerangTrap, ID)
	
		i_boomerangTrap.removeable = true --> Make sure this instance is/stays removeable!
		
		 --> Remove collider of all segments!
		_collider.HC:remove(i_boomerangTrap.collider)
		i_boomerangTrap.collider = nil
		
		--> Don't forget the boomerang!
		if i_boomerangTrap.boomerang then
			_boomerangTrap.removeBoomerang(i_boomerangTrap)
		end
		
		--> Remove entity out of instances table!
		table.remove(i_boomerangTrap.instances, ID)
	
	end,
	
	--> Removes a traps boomerang!
	removeBoomerang = function(i_boomerangTrap)
		if i_boomerangTrap.boomerang then
			i_boomerangTrap.boomerang.removeable = true
			 _collider.HC:remove(i_boomerangTrap.boomerang.collider)
			i_boomerangTrap.boomerang.collider = nil
			if i_boomerangTrap.boomerang.sound then
				_afx:stop(i_boomerangTrap.boomerang.sound, {fade = 0.5})
				i_boomerangTrap.boomerang.sound = nil
			end
			i_boomerangTrap.boomerang = nil
		end
	end,
	
	--> Removes all traps and their boomerangs!
	clear = function()
		for i=#_boomerangTrap.instances, 1, -1 do
			_boomerangTrap.removeBoomerang(_boomerangTrap.instances[i], i)
			 _collider.HC:remove(_boomerangTrap.instances[i].collider)
			_boomerangTrap.instances[i].collider = nil
			table.remove(_boomerangTrap.instances, i)
		end
	end,
	
	--> Handles collision of an instance!
	collide = function(i_boomerangTrap, i_other)
		if i_other.className and (i_other.className == "_line") then
			_boomerangTrap.throw(i_boomerangTrap, i_other.origin.x or i_other.pos[1], i_other.origin.y or i_other.pos[2])
		end
	end,
	
	--> Lets a boomerang trap throw its boomerang!
	throw = function(i_boomerangTrap, x, y)
		if not i_boomerangTrap.boomerang then
			_afx:play("enemyBoomerangTrapThrow.ogg", {x = i_boomerangTrap.x, y = i_boomerangTrap.y})
			_expandingCircle.new({x = i_boomerangTrap.x,
								  y = i_boomerangTrap.y,
								  r = _G.colours.danger.r,
								  g = _G.colours.danger.g,
								  b = _G.colours.danger.b,
								  a = 125,
								  radius = 0,
								  expandRate = 35,
								  lifetime = 1,
								  width = 4,
								  segments = 125,
								  degradation = 250,
								  rotation = math.rad(0)})
			i_boomerangTrap.boomerang = _boomerang.create({x = i_boomerangTrap.x,
										 y = i_boomerangTrap.y,
										 to = math.rad(math.atan2(y - i_boomerangTrap.y, x - i_boomerangTrap.x) * 180 / math.pi),
										 o = math.rad(love.math.random(1, 360)),
										 color = {_G.colours.danger.r, _G.colours.danger.g, _G.colours.danger.b, 255},
										 s = love.math.random(325, 375),
										 so = love.math.random(450, 550),
										 radius = 25,
										 width = 4,
										 originX = x,
										 originY = y,
										 })
		end
	end,
		
	--> Creates and returns a new instance!
	create = function(parameters)
	
		local ret = {}
		local p = parameters or {}

		--> General data!
		ret.className = "_boomerangTrap" --> className.collide() is used for collisions!
		ret.type = "enemy" --> Used for e.g. collision groups!

		ret.x = p.x or 300
		ret.y = p.y or 300
		
		ret.collider = _collider.HC:addCircle(ret.x, ret.y, 30)
		_collider.HC:addToGroup(ret.type, ret.collider)
		ret.collider.backLink = ret
		ret.boomerang = nil --> One trap can hold one boomerang!
		ret.removeable = false
				
		--> Finish creation, insert to instances and return!
		table.insert(_boomerangTrap.instances, ret)
		return ret
		
	end
}