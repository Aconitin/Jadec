return function(c_controller, keyTable) --> Creates a new controller for mouse and keyboard!

	local ret = {} --> Controller Instance!
	
	--> Set metatable!
	local mt = {
		__tostring = function(self)
			return "<Instance of class: _controller_MK. Can handle specific input keys.>"
		end
	}
	setmetatable(ret, mt)
	
	ret.callbacks = {} --> Holds (key, callback, parameters) tripels!
	ret.type = "mk"
	ret.active = true
	
	--> Check if the keyTable is valid!
	if not keyTable then
	_controller:debugPrint("KeyTable is not valid. Creating a default one!")
	end
	ret.keyTable = keyTable or {up = _G.keys.keyboardUp,
								down = _G.keys.keyboardDown,
								left = _G.keys.keyboardLeft,
								right = _G.keys.keyboardRight,
								sneak = _G.keys.keyboardSneak,
								running = _G.keys.keyboardRun,
								throwing = _G.keys.keyboardThrow, --> This is not a callback. When this is active, players are about to throw and can't move!
								throw = "ku" .. _G.keys.keyboardThrow, --> This callback actually triggers the throw!
								throwIdle = "kd" .. _G.keys.keyboardThrow, --> For idle!
								clap = "ku" .. _G.keys.keyboardClap,
								join = "kd" .. _G.keys.keyboardJoin, --> Key for joining the game. This is also a callback key!
								} --> Default keyTable. The controller instance will listen to these keys and return movementMatrices accordingly!
								
	--> Add getMovementMatrix function!
	ret.getMovementMatrix = function(i_controller)
		local ret = {upDown = 0, leftRight = 0}
		if love.keyboard.isDown(i_controller.keyTable.up) then
			ret.upDown = ret.upDown - 1
		end
		if love.keyboard.isDown(i_controller.keyTable.down) then
			ret.upDown = ret.upDown + 1
		end
		if love.keyboard.isDown(i_controller.keyTable.left) then
			ret.leftRight = ret.leftRight - 1
		end
		if love.keyboard.isDown(i_controller.keyTable.right) then
			ret.leftRight = ret.leftRight + 1
		end
		return ret
	end
	
	--> Registers a callback for this controller. Returns callback ID!
	ret.registerCallback = function(i_controller, callbackFunction, triggerKey, parameters, callbackID) --> Parameters is a table!
	_controller:debugPrint("Registered callback. Key = " .. triggerKey .. ", Name = " .. callbackID)
		i_controller.callbacks[callbackID] = {triggerKey = triggerKey, callbackFunction = callbackFunction, parameters = parameters}
	end
	
	--> Unregisters a callback for this controller!
	ret.unregisterCallback = function(i_controller, callbackID)
	_controller:debugPrint("Unregistered callback " .. callbackID)
		if i_controller.callbacks[callbackID] then
			i_controller.callbacks[callbackID] = nil
		end
	end
	
	--> Returns movementMatrix!
	ret.getFullUpdate = function(i_controller, playerX, playerY)
		local m = i_controller:getMovementMatrix()
		m.isSneaking = love.keyboard.isDown(i_controller.keyTable.sneak)
		m.isThrowing = love.keyboard.isDown(i_controller.keyTable.throwing)
		m.isRunning = love.keyboard.isDown(i_controller.keyTable.running)
		return m
	end
	
	_controller:debugPrint("Created controller (MK)!")
	
	--> Push instance back to main class!
	table.insert(c_controller.instances, ret)
	return c_controller:getController(c_controller:getCount())
end