local _menu = {

	iconEnter = love.graphics.newImage("gfx/iconEnter.png"),
	iconGuide = love.graphics.newImage("gfx/iconGuide.png"),
	joined = false,

	update = function(dt)
		_gamestates.updateGame(dt)
		--> Set texts!
		if not _gamestates["menu"].joined and #_player.instances ~= 0 then
			_gamestates["menu"].joined = true
			_map.clearText()
			local text = "Start\nGame"
			_map.newText(text, 11*70-35 - 0.5 * _map.fontSmall:getWidth(text), 3 * 70, _map.fontSmall, {255, 255, 255, 0}, 200, math.huge, nil)
			local text = "Main Menu"
			_map.newText(text, 6*70-35 - 0.5 * _map.fontSmall:getWidth(text), 1 * 70, _map.fontSmall, {255, 255, 255, 0}, 200, math.huge, nil)
			local text = "Quit\n "
			_map.newText(text, 0*70 - 0.5 * _map.fontSmall:getWidth(text), 3 * 70, _map.fontSmall, {255, 255, 255, 0}, 200, math.huge, nil)
		end
	end,
	

	draw = function()
		local cur = _gamestates[_gamestates.current]
		--> Draw everything via camera!
		cur.camera:draw(function(l, t, w, h)
			_gamestates.drawGame()
			if not cur.joined then
				--> Draw icons!
				love.graphics.setColor(255, 255, 255, _map.textInstances[2].color[4])
				love.graphics.draw(_gamestates.menu.iconEnter, -310, -295, 0, 0.15, 0.15)
				love.graphics.draw(_gamestates.menu.iconGuide, 20, -295, 0, 0.14, 0.14)
			end
				
		end)
	end,

	before = function()
		--> Setup controllers!
		_controller:setupControllersForAllInputDevices()
		--> Register the "create player" callbacks and the callbacks to start the game!
		for i,v in ipairs(_controller:getAllControllers()) do
			v:registerCallback(_player.create, v.keyTable.join, {{controller = v}}, "join")
		end
		_gamestates["menu"].music = _afx:play("Metaphysik.mp3", {loop=true})
		_map.next(true)
		
		--> Setup map text instances for main menu!
		local text = "Press      on keyboard or      on controller to join."
		_map.newText(text, - 0.5 * _map.fontSmall:getWidth(text), -300, _map.fontSmall, {255, 255, 255, 0}, 200, math.huge, nil)
		local text = "( Yes, local multiplayer is possible! )"
		_map.newText(text, - 0.5 * _map.fontSmall:getWidth(text), -300 + 1.5 * _map.fontSmall:getHeight(), _map.fontSmall, {255, 255, 255, 0}, 200, math.huge, nil)

	end
}
return _menu