local profiles = {

	--> The normal footstep!
	step = {
		nrOfLines = 24, --> How many lines there are in total!
		alpha = 150, --> How bright the footstep is (alpha value)!
		width = 3.5, --> How many pixels wide the lines will be!
		speed = 500, --> How fast the lines will move!
		lifetime = 1, --> How many seconds the lines will live before reaching full transparency!
	},

	--> The running footstep!
	run = {
		nrOfLines = 24, --> How many lines there are in total!
		alpha = 200, --> How bright the footstep is (alpha value)!
		width = 4.5, --> How many pixels wide the lines will be!
		speed = 650, --> How fast the lines will move!
		lifetime = 1.5, --> How many seconds the lines will live before reaching full transparency!
	},

	--> The footstep for sneaking in water (Normal sneaking does not have a footstep sound thingy)!
	sneakInWater = {
		nrOfLines = 24, --> How many lines there are in total!
		alpha = 100, --> How bright the footstep is (alpha value)!
		width = 2.5, --> How many pixels wide the lines will be!
		speed = 500, --> How fast the lines will move!
		lifetime = 1, --> How many seconds the lines will live before reaching full transparency!
	},
	
	--> Clap!
	clap = {
		nrOfLines = 48, --> How many lines there are in total!
		alpha = 200, --> How bright the footstep is (alpha value)!
		width = 4.5, --> How many pixels wide the lines will be!
		speed = 200, --> How fast the lines will move!
		lifetime = 7, --> How many seconds the lines will live before reaching full transparency!
	},
	
	--> Short, exhausted breath!
	shortBreath = {
		nrOfLines = 24, --> How many lines there are in total!
		alpha = 125, --> How bright the footstep is (alpha value)!
		width = 2.5, --> How many pixels wide the lines will be!
		speed = 650, --> How fast the lines will move!
		lifetime = 0.5, --> How many seconds the lines will live before reaching full transparency!
	},
	
	--> Long, final breath!
	longBreath = {
		nrOfLines = 24, --> How many lines there are in total!
		alpha = 150, --> How bright the footstep is (alpha value)!
		width = 3, --> How many pixels wide the lines will be!
		speed = 500, --> How fast the lines will move!
		lifetime = 1.5, --> How many seconds the lines will live before reaching full transparency!
	},

	--> Primary stone clack!
	stonePrimary = {
		nrOfLines = 24, --> How many lines there are in total!
		alpha = 125, --> How bright the footstep is (alpha value)!
		width = 1.5, --> How many pixels wide the lines will be!
		speed = 600, --> How fast the lines will move!
		lifetime = 1.75, --> How many seconds the lines will live before reaching full transparency!
	},
	
	--> Secondary stone clack!
	stoneSecondary = {
		nrOfLines = 32, --> How many lines there are in total!
		alpha = 112, --> How bright the footstep is (alpha value)!
		width = 1.25, --> How many pixels wide the lines will be!
		speed = 500, --> How fast the lines will move!
		lifetime = 1.25, --> How many seconds the lines will live before reaching full transparency!
	},
	
	--> Tertiary stone clack!
	stoneTertiary = {
		nrOfLines = 48, --> How many lines there are in total!
		alpha = 100, --> How bright the footstep is (alpha value)!
		width = 1, --> How many pixels wide the lines will be!
		speed = 400, --> How fast the lines will move!
		lifetime = 1, --> How many seconds the lines will live before reaching full transparency!
	},

}

return profiles