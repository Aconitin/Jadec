--> Intro gamestate!
local _intro = {
	update = function(dt)
		if not _gamestates.intro.started then
			_gamestates.intro.delay = _gamestates.intro.delay - dt
			if _gamestates.intro.delay < 0 then
				_gamestates.intro.video:play()
				_gamestates.intro.started = true
			end
		else
			local playing = _gamestates.intro.video:isPlaying()
			if not playing then
				_gamestates.switchTo("menu")
			end
		end
	end,
	
	draw = function()
		if _gamestates.intro.started then
			love.graphics.push()
			love.graphics.scale(love.graphics.getWidth()/1920, love.graphics.getHeight()/1080)
			love.graphics.draw(_gamestates.intro.video)
			love.graphics.pop()
		end
	end,
	
	before = function()
		if not _gamestates["intro"].video then --> This might have been preloaded by the splash!
			_gamestates["intro"].video = love.graphics.newVideo("gfx/Aconitin_Intro.ogv")
		end
		_gamestates.intro.started = false
		_gamestates.intro.delay = 1.5
		love.audio.setVolume(_G.configuration.masterVolume)
	end,
}
return _intro