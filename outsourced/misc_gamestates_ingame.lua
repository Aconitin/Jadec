local _ingame = {

	--> Update function!
	update = function(dt)
		local cur = _gamestates[_gamestates.current]
		_gamestates.updateGame(dt)
		--> Update game over condition!
		if _player.countAlivePlayers() == 0 then
			--> Wait a few seconds before making resetting the game!
			cur.resetCounter = cur.resetCounter + dt
			if cur.resetCounter >= 4 then
				cur.resetCounter = 0
				_map.next(true)
			end
		end
	end,
	
	--> Draw function!
	draw = function()
		local cur = _gamestates[_gamestates.current]
		--> Draw everything via camera!
		cur.camera:draw(function(l, t, w, h)
			_gamestates.drawGame()
		end)
	end,
	
	--> Before function gets called once when the gamestate is switched to this gamestate!
	before = function()
		_gamestates[_gamestates.current].camera = _gamestates["menu"].camera
		_gamestates[_gamestates.current].resetCounter = 0
		_afx:stop(_gamestates["menu"].music, {fade = 3})
		_afx:play("deepNoise.mp3", {loop=true})
		_afx:newScenery(10, 25, {"bgStimmung01.ogg", "bgStimmung02.ogg", "bgStimmung03.ogg", "bgStimmung04.ogg", "bgStimmung05.ogg", "bgStimmung06.ogg"}, -1)
		for i,v in pairs(_controller.instances) do
			v:unregisterCallback("join") --> Unregisters the "create player" callback of all controllers!
		end
	end
}
return _ingame