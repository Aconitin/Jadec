local tiles = { --> Definition of tiles!
	_air = {
		name = "_air",
		lineColor = _G.colours.normal,
		mapColor = {r = 255, g = 255, b = 255}, --> Colour that the map pixels are for this tile!
		debugDrawColor = {0, 255, 255, 50}, --> For debug draws only!
		footstepColor = _G.colours.normal,
		lineSizeFactor = 1,
		stepSoundLeft = "stepLeft.ogg",
		stepSoundRight = "stepRight.ogg",
	},
	_exit = {
		name = "_exit",
		lineColor = _G.colours.exit,
		mapColor = {r = 255, g = 255, b = 0},
		debugDrawColor = {255, 255, 0, 50}, --> For debug draws only!
		footstepColor = _G.colours.exit,
		lineSizeFactor = 2.5,
		onstepAction = function(i_player)
			if _gamestates.current == "menu" then
				_gamestates.switchTo("ingame")
			end
			_afx:play("exit.ogg")
			_map.next()
			_expandingCircle.new({x = _map.getMap().info.spawnpoint.x,
								  y = _map.getMap().info.spawnpoint.y,
								  r = _map.tiles["_exit"].lineColor.r,
								  g = _map.tiles["_exit"].lineColor.g,
								  b = _map.tiles["_exit"].lineColor.b,
								  a = 125,
								  radius = 0,
								  expandRate = 35,
								  lifetime = 2.25,
								  width = 2.5,
								  segments = 125,
								  degradation = 250,
								  rotation = math.rad(0)})
		end,
	},
	_wall = {
		name = "_wall",
		lineColor = {r = 123, g = 123, b = 123},
		mapColor = {r = 0, g = 0, b = 0},
		debugDrawColor = {0, 0, 0, 255}, --> For debug draws only!
		footstepColor = {}, --> Should never be used!
		lineSizeFactor = 0,
	},
	_water = {
		name = "_water",
		mapColor = {r = 0, g = 0, b = 255},
		lineColor = _G.colours.water,
		debugDrawColor = {0, 0, 255, 50}, --> For debug draws only!
		movementSpeedFactor = 0.5,
		footstepColor = _G.colours.water,
		stepSoundLeft = "splash01.ogg",
		stepSoundRight = "splash02.ogg",
		lineSizeFactor = 1.5,
		onstepAction = function(i_player)
			_afx:play("waterEnterLeave0" .. love.math.random(1, 3) .. ".ogg") --> Play a random splash sound!
			_line.createBunch("sneakInWater", {x = i_player.x, y = i_player.y})
		end,
		offstepAction = function(i_player)
			_afx:play("waterEnterLeave0" .. love.math.random(1, 3) .. ".ogg") --> Play a random splash sound!
			_line.createBunch("sneakInWater", {x = i_player.x, y = i_player.y})
		end,
	},
	_hole = {
		name = "_hole",
		mapColor = {r = 255, g = 0, b = 0},
		lineColor = _G.colours.danger,
		debugDrawColor = {255, 0, 0, 50}, --> For debug draws only!
		footstepColor = _G.colours.danger,
		lineSizeFactor = 1.5,
		onstepAction = function(i_player)
			if _gamestates.current == "menu" then
				_afx:play("death.ogg")
				for i,v in pairs(_player.instances) do
					i_player.dead = true
					i_player.controller.active = false
				end
				_gamestates.switchTo("leaving")
			else
				_player.die(i_player)
			end
		end,
	},
	
	_quitXit = {
		name = "_quitXit",
		lineColor = _G.colours.exit,
		mapColor = {r = 254, g = 255, b = 0},
		debugDrawColor = {255, 255, 0, 50}, --> For debug draws only!
		footstepColor = _G.colours.exit,
		lineSizeFactor = 2.5,
		onstepAction = function(i_player)
			for i,v in pairs(_player.instances) do
					i_player.dead = true
					i_player.controller.active = false
				end
		love.audio.stop( )
			_gamestates.switchTo("leaving")
		end,
	},
}

return tiles