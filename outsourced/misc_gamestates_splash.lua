--> Splash gamestate, for preloading resources and generating maps!
local _splash = {

	preLoad = function(dt) --> Preload resources while in splash screen!
				
		if _G.configuration.preBufferData then
			coroutine.yield()
			local music1 = _afx:play("deepNoise.mp3", {volume = 0.0})
			_afx:stop(music1)
			-->
			coroutine.yield()
			local music2 = _afx:play("Metaphysik.mp3", {volume = 0.0})
			_afx:stop(music2)
			-->
			coroutine.yield()
			local sfx1 = _afx:play("enemyWhispLoop.ogg", {volume = 0.0})
			_afx:stop(sfx1)
			-->
			coroutine.yield()
			local music3 = _afx:play("Paul Ressel - Block Out the Sun.mp3", {volume = 0.0})
			_afx:stop(music3)
			-->
			coroutine.yield()
			_gamestates["intro"].video = love.graphics.newVideo("gfx/Aconitin_Intro.ogv")
			-->
		end

		while #_map.instances ~= _map.minMaxInstances do
			_map.updateMapCreation()
			coroutine.yield()
		end
		
		_gamestates["splash"].preBuffered = true
		
	end,
	
	update = function(dt)
	
		--> Make things easier for all of us!
		local cur = _gamestates[_gamestates.current]
		
		--> Create preloading coroutine!
		if not cur.cor then
			cur.cor = coroutine.create(cur.preLoad)
		end
		
		--> Update the coroutine!
		if not _gamestates["splash"].preBuffered then
			coroutine.resume(cur.cor, dt)
		else
			love.window.setMode(_G.configuration.xResolution, _G.configuration.yResolution, {fullscreen = _G.configuration.fullscreen,
																							 fullscreentype = _G.configuration.fullscreentype,
																							 display = _G.configuration.display,
																							 highdpi = _G.configuration.highdpi,
																							 vsync = _G.configuration.vsync,
																							 msaa = _G.configuration.msaa,
																							 resizable = false,
																							 borderless = false,
																							 centered = true})
			if _G.configuration.skipIntro then
				_gamestates.switchTo("menu")
			else
				_gamestates.switchTo("intro")
			end
		end
		
	end,

	draw = function()
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.setFont(_map.fontSmall)
		love.graphics.print("Loading...", 0.5 * (love.graphics.getWidth() - _map.fontSmall:getWidth("Loading...")), 0.5 * (love.graphics.getHeight() - _map.fontSmall:getHeight()))
	end,

	before = function()
		love.window.setIcon(love.image.newImageData("gfx/icon.png"))
		love.mouse.setVisible(false)
		love.graphics.setBackgroundColor(0, 0, 0, 255)
	end
	
}

return _splash