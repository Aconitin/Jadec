--> Gamestate responsible for playing the credits and finishing the game afterwards!
local _leaving = {

	--> Update function!
	update = function(dt)
	
		--> Update state!
		local cur = _gamestates[_gamestates.current]
		_afx:update(dt)
		cur.time = cur.time + dt
		
		--> Update level texts!
		for i=#_map.textInstances, 1, -1 do
			local v = _map.textInstances[i]
			_map.updateText(v, dt)
			if v.removeable then
				table.remove(_map.textInstances, i)
			end
		end
		
		--> Play music after 3 seconds and add header!
		if cur.time >= 3 and not cur.triggers[1] then
			cur.triggers[1] = true
			_afx:play("Paul Ressel - Block Out the Sun.mp3")
			_map.newText("Jadec", love.graphics.getWidth()/2 - _map.fontBig:getWidth("Jadec")/2, 140, _map.fontBig, {255, 255, 255, 0}, 200, 100, nil)
		end
		
		--> After 8 seconds, start moving things!
		if cur.time >= 11.5 then
			for i,v in ipairs(_map.textInstances) do
				v.y = v.y - dt * 20
			end
		end
		
		--> Add subheader!
		if cur.time >= 7.5 and not cur.triggers[4] then
			cur.triggers[4] = true
			local text = "(Just A 'Dark Echo Clone')"
				_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight(), _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
		end
		
		--> A game by me!
		if cur.time >= 11.5 and not cur.triggers[3] then
			cur.triggers[3] = true

			local text = "A game by Aconitin"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * 2, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local text = "( @aconitin_ )"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * 3, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local text = "Made in 2015/2016 with Lua & Löve2d"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * 5, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local text = "Special thanks to:"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * 7, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = 8.5
			local text = "rac7 (Dark Echo)"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth("Special thanks to:")/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Enrique García Cota (Gamera library)"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Matthias Richter (HardonCollider library)"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "mlnlover11 (Roman Numerals library)"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
					
			local factor = factor + 1
			local text = "@Catlinman_ (Intro animation & being awesome)"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Kevin MacLeod (Ingame music)"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Paul Ressel (Outro music (this one))"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Freesound.org (Sound effects)"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 2
			local text = "All the people in the #cobalt, #lua & #love IRC channels:"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "APirateHat"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Bartbes"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "BobbyJones"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Catlinman"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "DesertRock"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "DolanCZ"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Gelicia"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Frall"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Holo"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Isogash"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Janeator <-- Pleb."
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth("Janeator")/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Jokler"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Kinten"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Linard"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "MasterGeek"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "RayKatz"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
							
			local factor = factor + 1
			local text = "Slime"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Shocky3 <-- Bot. Still cooler than APirateHat."
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth("Shocky3")/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Thewreck"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 1
			local text = "Tundmatu"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)

			local factor = factor + 1
			local text = "(...and many more! Thanks for your feedback and support!)"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
			local factor = factor + 2
			local text = "And to you, the player!"
			_map.newText(text, love.graphics.getWidth()/2 - _map.fontSmall:getWidth(text)/2, 140 + _map.fontBig:getHeight() + _map.fontSmall:getHeight() * factor, _map.fontSmall, {255, 255, 255, 0}, 200, 100, nil)
			
		end

		--> Quit game after maximum maxTime seconds (1:40?)!
		if cur.time >= cur.maxTime and not cur.triggers[2] then
			cur.triggers[2] = true
			love.event.quit()
		end
		
	end,
	
	--> Draw function!
	draw = function()
		--> Draw map texts above everything!
		for i,v in ipairs(_map.textInstances) do
			love.graphics.setColor(v.color)
			love.graphics.setFont(v.font)
			love.graphics.print(v.text, v.x, v.y)
		end
	end,
	
	--> Before function gets called once when the gamestate is switched to this gamestate!
	before = function()
		_afx:stop(_gamestates["menu"].music)
		_map.textInstances = {}
		_line.instances = {}
		_map.instances = {}
		_gamestates["leaving"].time = 0
		_gamestates["leaving"].triggers = {false, false, false, false}
		_gamestates["leaving"].maxTime = 100 --> After 1:40, the song is finished!
			
	end,
	
	keypressed = function(a)
	print(a)
		if a == "escape" then
			love.event.quit()
		end
	end,

	mousepressed = function()
	end,
	
	gamepadpressed = function()
	end,

}
return _leaving