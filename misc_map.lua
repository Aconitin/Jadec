_map = { --> Merged class for _map and _level. Handles map creation, application, choosing, etc!

	--[[
		Example of a map instance:
		map.tiles[x][y] = "_wall", where x and y are not coordinates!
		map.info = {}, which holds information about where the spawnpoints are etc!
		map.spriteBatch, holds all wall tiles (those get drawn!)!
	--]]

	--> Parametrisation!
	tileSize = 35, --> Global size of all tiles!
	mapsFolder = "maps/", --> Folder where the maps are saved in!
	minMaxInstances = 5,
	
	--> Variables!
	level = 0, --> Current level the game is on!
	fontBig = love.graphics.newFont("gfx/Russo_One.ttf", 70),
	fontSmall = love.graphics.newFont("gfx/Russo_One.ttf", 35),
	fontTiny = love.graphics.newFont("gfx/Russo_One.ttf", 20),
	textInstances = {},
	instances = {}, --> instances[1] is always the current map!
	creation = {cor = nil, createdLevel = -1, curName = nil}, --> Coroutine and info to create new maps!
	
	--> Definition of tiles!
	tiles = nil, --> Required in main!
	
	--[[
		Most important functions!
	--]]
	
	--> Pushes next level!
	next = function(resetOnly)
		
		--> If there is no new map, force making one. Normally this is done by update(), but in the rare event that isn't fast enough, this forces the process!
		while #_map.instances <= 1 do
			_map.update(0) --> Keep in mind this has no dt!
		end
		
		_gamestates.clearGame()
		
		--> Load the new map by removing the old one. Do not remove the old one if the current level is 0!
		if not resetOnly then
			table.remove(_map.instances, 1)
			_map.level = _map.level + 1
		end

		--> Revive all dead players and set their new spawnpoint!
		for i,v in ipairs(_player.instances) do
			_player.revive(v)
			_player.setPosition(v, _map.getMap().info.spawnpoint.x, _map.getMap().info.spawnpoint.y)
		end

		--> Setup the camera!
		if not _gamestates["menu"].camera then
			_gamestates["menu"].camera = _gamera.new(-1920, -1080, _map.getWidth() + 2*1920 , _map.getHeight() + 2*1080)
		else
			_gamestates["menu"].camera:setWorld(-1920, -1080, _map.getWidth() + 2*1920 , _map.getHeight() + 2*1080)
		end
		
		--> Spawn enemies!
		for i,v in pairs(_map.getMap().info.enemies) do
			if v.type == "whisp" then
				_whisp.create({x = v.x, y = v.y, s = v.s})
			elseif v.type == "boomerangTrap" then
				_boomerangTrap.create({x = v.x, y = v.y})
			elseif v.type == "lindworm" then
				_lindworm.create({x = v.x, y = v.y, minX = 0, minY = 0, maxX = _map.getWidth(), maxY = _map.getHeight()})
			end
		end
		
		--> Draw text and subtitle!
		if _map.level ~= 0 then
			assert(_map.instances[1].info.title and _map.instances[1].info.subtitle and _map.instances[1].info.subsubtitle, "Error: _map has no title/subtitle/subsubtitle!")
			local text = _romanNumerals.toRomanNumerals(_map.level) .. " - " .. _map.getMap().info.title
			_map.newText(text, (_map.getMap().info.spawnpoint.x - 0.5 * _map.fontBig:getWidth(text)), _map.getMap().info.spawnpoint.y - 300, _map.fontBig, {255, 255, 255, 0}, 200, 3, {fin = 2, fout = -3})
			_map.newText(_map.instances[1].info.subtitle, (_map.getMap().info.spawnpoint.x - 0.5 * _map.fontSmall:getWidth(_map.instances[1].info.subtitle)), _map.getMap().info.spawnpoint.y - 300 + 1 * _map.fontBig:getHeight(), _map.fontSmall, {255, 255, 255, 0}, 200, 4, {fin = 2, fout = -3})
			_map.newText(_map.instances[1].info.subsubtitle, (_map.getMap().info.spawnpoint.x - 0.5 * _map.fontTiny:getWidth(_map.instances[1].info.subsubtitle)), _map.getMap().info.spawnpoint.y - 300 + 1.5 * _map.fontBig:getHeight(), _map.fontTiny, {255, 255, 255, 0}, 200, 5, {fin = 2, fout = -3})
		end
		
	end,
	
	updateMapCreation = function()
		_map.update(1, true)
	end,
	
		
	--> Updates the mapmaking process. Always keeps the number of already-generated maps at 3!
	update = function(dt, preloadOnly)
	
		--> Make new maps and keep the coroutines running!
		if not _map.creation.cor and #_map.instances < _map.minMaxInstances then
			--> Make a new map-making coroutine!
			_map.creation.createdLevel = _map.creation.createdLevel + 1 --> Level that is now to be created!
			if _G.maps.ordered[_map.creation.createdLevel] then --> If there is an ordered level, take that one!
				_map.creation.curName = _G.maps.ordered[_map.creation.createdLevel]
				_map.creation.cor = coroutine.create(_map.createFromFiles)
			else --> If there ain't no ordered level, check if there are unplayed unordered ones!
				local found = false
				for i,v in pairs(_G.maps.unordered) do
					if v == true then
						found = true
						_map.creation.curName = i
						_G.maps.unordered[i] = false
						break
					end
				end
				if found then --> Finally, as last measure, create a random map from scratch!
					_map.creation.cor = coroutine.create(_map.createFromFiles)
				else
					_map.creation.cor = coroutine.create(_map.createRandom)
				end
			end
		end
		
		--> Check on the coroutine and keep it running!
		if _map.creation.cor and coroutine.status(_map.creation.cor) == "suspended" then
			coroutine.resume(_map.creation.cor, _map.creation.curName)
		elseif _map.creation.cor and coroutine.status(_map.creation.cor) == "dead" then
			_map.creation.cor = nil
			_map.creation.curName = nil
		end	
		
		if not preloadOnly then
			
			--> Update level texts!
			for i=#_map.textInstances, 1, -1 do
				local v = _map.textInstances[i]
				_map.updateText(v, dt)
				if v.removeable then
					table.remove(_map.textInstances, i)
				end
			end
			
			--> Make tile effects on water!
			if _map.instances[1] and _map.instances[1]["_water"] and #_map.instances[1]["_water"] ~= 0 then
				if love.math.random(1, love.timer.getFPS()) == 1 then --> Small chance for effect!
					local v = _map.instances[1]["_water"][love.math.random(1, #_map.instances[1]["_water"])]
					local n = love.math.random(1, 4) --> Choose random drop sound!
					local x = v.x * _map.tileSize - love.math.random(1, _map.tileSize)
					local y = v.y * _map.tileSize - love.math.random(1, _map.tileSize)
					local r, g, b = _map.getCurrentTileRGB(x, y)
					_afx:play("waterDrop0" .. n .. ".ogg", {x = x, y = y})
					_expandingCircle.new({x = x, y = y, r = r, g = g, b = b, a = 255, radius = 0, expandRate = 35, lifetime = 0.5, degradation = 510})
				end
			end
			
		end

	end,
	
	--> Draws the map!
	draw = function()
	
		--> If debug is on, draw map!
		if __drawColliders then
			for x, v1 in pairs(_map.instances[1].tiles) do
				for y, v in pairs(v1) do
					if v ~= "_wall" then
						love.graphics.setColor(_map.tiles[v].debugDrawColor)
						love.graphics.rectangle("fill", (x-1)*_map.tileSize, (y-1)*_map.tileSize, _map.tileSize, _map.tileSize)
					end
				end
			end
		end
		
		--> Draw map walls in every case!
		love.graphics.setColor(0, 0, 0, 255)
		love.graphics.draw(_map.getMap().spriteBatch, 0, 0)
		
		--> Draw map texts above everything!
		for i,v in ipairs(_map.textInstances) do
			love.graphics.setColor(v.color)
			love.graphics.setFont(v.font)
			love.graphics.print(v.text, v.x, v.y)
		end
		
	end,

	updateText = function(i_text, dt)
		if i_text.fade.fin > 0 then
			i_text.fade.fin = i_text.fade.fin - dt
			if i_text.fade.fin < 0 then
				i_text.fade.fin = 0
			end
			i_text.color[4] = i_text.color[4] + dt*i_text.fin
			if i_text.color[4] > i_text.maxAlpha then
				i_text.color[4] = i_text.maxAlpha
			end
		elseif i_text.dur > 0 then
			i_text.dur = i_text.dur - dt
		elseif i_text.fade.fout < 0 then
			i_text.fade.fout = i_text.fade.fout + dt
			if i_text.fade.fout > 0 then
				i_text.fade.fout = 0
			end
			i_text.color[4] = i_text.color[4] + dt*i_text.fout
			if i_text.color[4] < 0 then
				i_text.color[4] = 0
			end
		else
			i_text.removeable = true
		end
	end,
	
	--[[
		Auxiliary functions!
	--]]
		
	--> Returns current line color by player position!
	getCurrentTileRGB = function(x, y) --> Gets tile color by (player) position!
		local currentTile = _map.getTileAt(x, y)
		if currentTile then
			return _map.tiles[currentTile].lineColor.r, _map.tiles[currentTile].lineColor.g, _map.tiles[currentTile].lineColor.b
		end
		assert(false, "_map currentTile is nil")
	end,
	
	--> Returns current line size factor by player position!
	getCurrentTileLineSizeFactor = function(x, y) --> Gets tile color by (player) position!
		local currentTile = _map.getTileAt(x, y)
		if currentTile then
			return _map.tiles[currentTile].lineSizeFactor
		end
		assert(false, "_map currentTile is nil")
	end,
			
	--> Returns current map width!
	getWidth = function()
		return _map.instances[1].info.width
	end,
	
	--> Returns current map height!
	getHeight = function()
		return _map.instances[1].info.height
	end,
	
	--> Returns current map!
	getMap = function()
		return _map.instances[1]
	end,
			
	--> For collision detection. Uses player position!
	isBlocked = function(x, y)
		local tile = _map.getTileAt(x, y)
		if tile then
			if tile ~= "_wall" then --> A wall is at the moment the only thing that blocks movement!
				return false
			end
			return true
		end
		return false --> Can walk on void!
	end,		
		
	--> Returns name of tile at (player) coordinates or nil if out of area!
	getTileAt = function(x, y)
		local mx, my = _map.getMapPosAt(x, y)
		if _map.instances[1].tiles[mx] and _map.instances[1].tiles[mx][my] then
			return _map.instances[1].tiles[mx][my]
		end
		assert(false, "_map getTileAt() tile is out of bounds!")
	end,	

	--> Converts coordinates (500, 500) to tile numbers (23,23)!
	getMapPosAt = function(x, y)
		return 1+math.floor(x/_map.tileSize), 1+math.floor(y/_map.tileSize)
	end,
	
	--> Draws a text somewhere!
	newText = function(text, x, y, font, color, maxAlpha, dur, fade)
		local ret = {}
		ret.text = text or "NoText"
		ret.x = x or 0
		ret.y = y or 0
		ret.dur = dur or 3 --> Duration!
		ret.font = font or nil --> Required!
		ret.color = color or {255, 255, 255, 0}
		ret.maxAlpha = maxAlpha or 200
		ret.fade = fade or {fin = 2, fout = -3}
		ret.fin = ret.maxAlpha/ret.fade.fin
		ret.fout = ret.maxAlpha/ret.fade.fout
		ret.removeable = false
		table.insert(_map.textInstances, ret)
	end,
	
	clearText = function()
		for i=#_map.textInstances,1,-1 do
			_map.textInstances[i] = nil	
		end
		_map.textInstances = {}
	end,
	
	--[[
		Mapmaking functions (coroutines)!
	--]]
	
	--> Loads a map from .png and .info files (coroutine!)!
	createFromFiles = function(mapName)
	
		--> Initialize!
		local imageData = love.image.newImageData(_map.mapsFolder .. mapName .. ".png")
		local map = {tiles = {}, info = {width = imageData:getWidth()*_map.tileSize, height = imageData:getHeight()*_map.tileSize, enemies = {}}}
		local defaultTranslation = "_air"
		local translate = function(r, g, b) --> From RGB to appropriate map tile!
			for i,v in pairs(_map.tiles) do
				if r == v.mapColor.r and g == v.mapColor.g and b == v.mapColor.b then
					return i
				end
			end
			return defaultTranslation
		end
		local splitString = function(str, del)
			local ret = {}
			for v in string.gmatch(str, del) do
				table.insert(ret, v)
			end
			return ret
		end
		coroutine.yield()
		
		--> Create Sprite Batch for the _walls (only tile that actually gets drawn)!
		map.spriteBatch = love.graphics.newSpriteBatch(love.graphics.newImage((function() 
			local imageData = love.image.newImageData(_map.tileSize, _map.tileSize)
			for x=0, _map.tileSize-1, 1 do
				for y=0, _map.tileSize-1, 1 do
					imageData:setPixel(x, y, 0, 0, 0, 255)
				end
			end
			return imageData
		end)()), 1)
		coroutine.yield()
		
		--> Transfer imageData to map data!
		for x=1,imageData:getWidth(),1 do
			for y=1,imageData:getHeight(),1 do
				local r, g, b, a = imageData:getPixel(x-1, y-1)
				local tile = translate(r, g, b)
				
				--> Make tile entry based on coordinates!
				if not map.tiles[x] then
					map.tiles[x] = {}
				end
				map.tiles[x][y] = tile
				
				--> Make tile entry based on tile type!
				if not map[tile] then
					map[tile] = {}
				end
				table.insert(map[tile], {x = x, y = y})
				
				--> Add tiles to the spriteBatch to draw the walls!
				if tile == "_wall" then --> Wall has to be drawn in a spriteBatch!
					map.spriteBatch:setBufferSize(map.spriteBatch:getBufferSize() + 1)
					map.spriteBatch:add((x-1)*_map.tileSize, (y-1)*_map.tileSize)
				end
				
			end
			coroutine.yield()
		end
		
		--> Get map info!
		for line in love.filesystem.lines(_map.mapsFolder .. mapName .. ".info") do
			local values = splitString(line, "([^ ]+)")
			if values[1] == "player" then
				map.info.spawnpoint = {x = (values[2]-1)*_map.tileSize + 0.5*_map.tileSize, y = (values[3]-1)*_map.tileSize + 0.5*_map.tileSize}
			elseif values[1] == "whisp" then				
				table.insert(map.info.enemies, {type = "whisp", x = (values[2]-1)*_map.tileSize, y = (values[3]-1)*_map.tileSize, s = values[4]})
			elseif values[1] == "boomerangTrap" then				
				table.insert(map.info.enemies, {type = "boomerangTrap", x = (values[2]-1)*_map.tileSize, y = (values[3]-1)*_map.tileSize})
			elseif values[1] == "lindworm" then				
				table.insert(map.info.enemies, {type = "lindworm", x = (values[2]-1)*_map.tileSize, y = (values[3]-1)*_map.tileSize})
			elseif values[1] == "title" then
				local title = ""
				for i,v in ipairs(values) do
					if i ~= 1 then
						title = title .. v
						if i ~= #values then
							title = title .. " "
						end
					end
				end
				map.info.title = title
			elseif values[1] == "subtitle" then
				local subtitle = ""
				for i,v in ipairs(values) do
					if i ~= 1 then
						subtitle = subtitle .. v
						if i ~= #values then
							subtitle = subtitle .. " "
						end
					end
				end
				map.info.subtitle = subtitle
			elseif values[1] == "subsubtitle" then
				local subsubtitle = ""
				for i,v in ipairs(values) do
					if i ~= 1 then
						subsubtitle = subsubtitle .. v
						if i ~= #values then
							subsubtitle = subsubtitle .. " "
						end
					end
				end
				map.info.subsubtitle = subsubtitle
			end
			
			--> If there ain't no title, add one!
			if not map.info.title then
				map.info.title = "Random Map"
			end
			--> If there ain't no subtitle, add one!
			if not map.info.subtitle then
				map.info.subtitle = _map.subtitles[love.math.random(1, #_map.subtitles)]
			end
			--> If there ain't no subsubtitle, add one!
			if not map.info.subsubtitle then
				map.info.subsubtitle = ""
			end
		end
			
		--> Add map to instances
		table.insert(_map.instances, map)
		
	end,
	
	--> Creates a random new map (coroutine!)!
	createRandom = function()
	
		--> Parameters!
		minfloorsLength = 500
		maxfloorsLength = 750
		avgDirChange = 1
		
		--> Instances!
		local map = {}
		local addColumn = function(tbl, location)
			if location == "before" then
				table.insert(tbl, 1, {})
				return tbl
			elseif location == "after" then
				table.insert(tbl, {})
				return tbl
			end		
		end
		
		local floors = {{}}  --> The table that holds the floors. It is initialized as 1*1.
		local current = {x = 1, y = 2}  --> The x/y indices of the floor that is currently generated.
		local dir = love.math.random(1, 4)  --> math.random(i, j) takes two integers and floorsurns a (pseudo-)random number in between them.
		local upperBound = love.math.random(minfloorsLength, maxfloorsLength)  --> The maximum number of iterations.
		local i = 1  --> A counter.
		
		while true do  --> Main loop used to generate the dungeon.
			if i > upperBound then
				break  --> Exit the loop when the dungeon is finished.
			end
			floors[current.x][current.y] = "_air"  --> Mark the current square.
			i = i + 1 --> Raise the counter.
			if love.math.random(1, avgDirChange) == 1 then
				dir = love.math.random(1, 4)  --> Change the direction: 1=up, 2=down, 3=left, 4=right.
			end
			local new = {}  --> Stores the x/y coordinates of the new floor/square.
			if dir == 1 then --> If the current direction is upwards, proceed.
				if current.y > 2 then  --> This makes sure you do not try to access a table field that is out of range.
					new = {x = current.x, y = current.y - 1}  --> If the new field is in range, store the new x/y field coordinates.
				else
					new = {x = current.x, y = current.y}  --> Afterwards, store the new x/y field coordinates.
				end
			elseif dir == 2 then  --> Do the same with each of the four directions.
				if current.y < #floors[current.x] then
					new = {x = current.x, y = current.y + 1}
				else
					new = {x = current.x, y = current.y + 1}
				end
			elseif dir == 3 then
				if current.x > 1 then
					new = {x = current.x - 1, y = current.y}
				else
					floors = addColumn(floors, "before")
					new = {x = current.x, y = current.y}
				end
			elseif dir == 4 then
				if current.x < #floors then
					new = {x = current.x + 1, y = current.y}
				else
					floors = addColumn(floors, "after")
					new = {x = current.x + 1, y = current.y}
				end
			end
			current = {x = new.x, y=new.y} --> Update the current x/y field coordinates.
			if i%100==0 then
				coroutine.yield()
			end
		end
		table.insert(floors, {})
		table.insert(floors, 1, {})

		--> Create map borders (walls as outlines for the air)!
		local newFloors = {}
		local i = 0
		for i1, v1 in pairs(floors) do
			for i2, v2 in pairs(v1) do
				i = i + 1
				if not newFloors[i1] then
					newFloors[i1] = {}
				end
				newFloors[i1][i2] = v2
				local n = {}
				table.insert(n, {x = i1-1, y = i2-1})
				table.insert(n, {x = i1-1, y = i2+1})
				table.insert(n, {x = i1-1, y = i2})
				table.insert(n, {x = i1, y = i2-1})
				table.insert(n, {x = i1, y = i2+1})
				table.insert(n, {x = i1+1, y = i2-1})
				table.insert(n, {x = i1+1, y = i2})
				table.insert(n, {x = i1+1, y = i2+1})
				for i=#n, 1, -1 do
					if not newFloors[n[i].x] then
						newFloors[n[i].x] = {}
					end
					if not floors[n[i].x][n[i].y] then		
						newFloors[n[i].x][n[i].y] = "_wall"
					end	
				end
				if i%100==0 then
					coroutine.yield()
				end
			end
		end
		floors = nil
		collectgarbage()
		map.floors = newFloors
		coroutine.yield()
	
		--> Create Sprite Batch for the _walls (only tile that actually gets drawn)!
		local ret = {tiles = {}, info = {enemies = {}}}
		ret.spriteBatch = love.graphics.newSpriteBatch(love.graphics.newImage((function() 
			local imageData = love.image.newImageData(_map.tileSize, _map.tileSize)
			for x=0, _map.tileSize-1, 1 do
				for y=0, _map.tileSize-1, 1 do
					imageData:setPixel(x, y, 0, 0, 0, 255)
				end
			end
			return imageData
		end)()), 1)
		coroutine.yield()

		--> Copy data to a new, clean map and fill the spriteBatch!
		for x, v1 in pairs(map.floors) do
			for y, v2 in pairs(v1) do
				if not ret.tiles[x] then
					ret.tiles[x] = {}
				end
				ret.tiles[x][y] = v2
				--> Add tiles to the spriteBatch to draw the walls!
				if v2 == "_wall" then --> Wall has to be drawn in a spriteBatch!
					ret.spriteBatch:setBufferSize(ret.spriteBatch:getBufferSize() + 1)
					ret.spriteBatch:add((x-1)*_map.tileSize, (y-1)*_map.tileSize)
				end
			end
		end
	
		--> Calculate map sizes!
		local maxX = 1
		local maxY = 1
		for i,v in pairs(map.floors) do
			maxX = maxX + 1
			for i2,v2 in pairs(v) do
				if i2 > maxY then
					maxY = i2
				end
			end
		end
		ret.info.width = maxX*_map.tileSize
		ret.info.height = maxY*_map.tileSize
		coroutine.yield()

		--> Add a spawnpoint for the player to the map!
		while true do
			local x, y = love.math.random(1, ret.info.width), love.math.random(1, ret.info.height)
			if ret.tiles[x] and ret.tiles[x][y] and ret.tiles[x][y] == "_air" then
				ret.info.spawnpoint = {x = (x-1)*_map.tileSize+0.5*_map.tileSize, y = (y-1)*_map.tileSize+0.5*_map.tileSize}
				break
			end
		end
		
		--> Add a subtitle!
		ret.info.subtitle = _map.subtitles[love.math.random(1, #_map.subtitles)]

		--> Add map to instances
		table.insert(_map.instances, ret)
	end,
	
	subtitles = { --> Just for fun. The possible subtitles below the "Level x" text!
		"Get Ready!",
		"Easy Peasy!",
		"Lemon squeezy!",
		"Now with extra juice!",
		"Uh-oh...",
		"You got this!",
		"Don't die again!",
		"Piece of cake!",
		"You're gonna f*ck up this one...",
		"Be nice to the enemies!",
		"Approved by Kim Jong-un!",
		"Let's add some invisible wormholes! j/k",
		"No, we won't skip that level!",
		"Player, meet hunter. Hunter, meet player.",
		"Probably not the last level.",
		"The exit has been removed, hue hue!",
		"You think you're lucky, punk?",
		"Probably not level 12.",
		"Janeator didn't get that far!",
		"Release the kraken!",
		"<Insert witty comment here>",
		"Did you know cats only last 20 seconds out of earth's atmosphere?",
		"Don't mind the holes!",
		"Ask your doctor if this level is good for you!",
		"wtf",
		"If you didn't have second thoughts, now you will!",
		"Read the manual for risks and sideeffects!",
		"There is no easter egg in this level!",
		"Have you found the Easter egg yet?",
		"Do you think you're good enough?",
		"Man, you suck at this game...",
		"You rock!",
		"We don't recycle assets!",
		"Run, forest, ruuuuuuun!",
		"Better than Dark Echo!",
		"Behind you!",
		"Now with 14% more empty space!",
		"We replaced your stones with bacon. Everybody loves bacon!",
		"Fire away!",
		"Is that...ohnonononono",
		"Trust me, this level is awful",
		"Do you feel significant?",
		"Let's jam!",
		"My favourite level!",
		"I don't have time to bleed!",
		"You're gonna stand in line soon!",
	},
	
}