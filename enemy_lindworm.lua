_lindworm = {

	--[[ Info
	
		Uses: _collider, _afx
		
		drawAll()
		updateAll(dt)
		draw(i_lindworm)
		remove(i_lindworm, ID)
		clear() --> Removes all lindworm instances!
		update(i_lindworm, dt)
		create(parameters)

	--]]

	--> Holds all lindworm instances!
	instances = {},
	
	--> Draws all lindworm instances!
	drawAll = function()
		for i,v in pairs(_lindworm.instances) do
			_lindworm.draw(v)
		end
	end,
	
	--> Updates all lindworm instances!
	updateAll = function(dt)
		for i=#_lindworm.instances,1,-1 do --> Backwards iteration!
			local v = _lindworm.instances[i]
			if v.removeable then
				_lindworm.remove(v, i) --> Remove lindworm if removeable!
			else
				_lindworm.update(v, dt) --> Update given lindworm if not removeable!
			end
		end
	end,
		
	--> Draws one given lindworm instance!
	draw = function(i_lindworm)
		for i,v in pairs(i_lindworm.segments) do --> Draw all segments of a lindworm individually!
			love.graphics.setColor(i_lindworm.r, i_lindworm.g, i_lindworm.b, i_lindworm.a)
			love.graphics.draw(v.particleSystem, v.x, v.y)
			if __drawColliders then --> Debug draw!
				love.graphics.setColor(99, 99, 99, 150)
				v.collider:draw()
			end
		end
	end,
	
	--> Removes one given lindworm instance!
	remove = function(i_lindworm, ID) --> ID is the index in the instances table!
	
		i_lindworm.removeable = true --> Make sure this instance is/stays removeable!
		
		 --> Remove collider of all segments!
		for i,v in pairs(i_lindworm.segments) do
			_collider.HC:remove(v.collider)
			v.backlink = nil
		end		
		
		_afx:stop(i_lindworm.sound, {fade = 1})
		
		--> Remove entity out of instances table!
		table.remove(_lindworm.instances, ID)
	end,
	
	--> Removes all lindworm instances!
	clear = function()
		for i=#_lindworm.instances,1,-1 do --> Backwards iteration!
			_lindworm.remove(_lindworm.instances[i], i) --> Remove lindworm if removeable!
		end
		assert(#_lindworm.instances == 0, "Error: _lindworm instances is not zero after a clear!")
	end,
		
	--> Updates one given lindworm instance!
	update = function(i_lindworm, dt)

		--> New target if it is too close!
		local angleAddition = 0
		while not i_lindworm.target or (math.abs(i_lindworm.segments[1].x - i_lindworm.target.x) <= i_lindworm.targetFoundRadius and math.abs(i_lindworm.segments[1].y - i_lindworm.target.y) <= i_lindworm.targetFoundRadius) do
			local targetRange = love.math.random(i_lindworm.maxTargetRange, i_lindworm.minTargetRange)
			local targetAngle = i_lindworm.o + math.rad(love.math.random(i_lindworm.minTargetAngle, i_lindworm.maxTargetAngle)) + math.rad(angleAddition)
			local x = i_lindworm.segments[1].x + targetRange * math.cos(targetAngle)
			local y = i_lindworm.segments[1].y + targetRange * math.sin(targetAngle)
			if x > i_lindworm.minX and x < i_lindworm.maxX and y > i_lindworm.minY and y < i_lindworm.maxY then
				i_lindworm.target = {x = x, y = y}
				break
			else
				angleAddition = angleAddition + 1
			end
		end

		--> Update movement!
		local too = math.rad(math.atan2(i_lindworm.target.y - i_lindworm.y, i_lindworm.target.x - i_lindworm.x) * 180 / math.pi)
		local factor = 1
		local pi = math.pi
		local angle1, angle2 = i_lindworm.o, too
		local diff = angle2 - angle1
		diff = diff > pi and diff - 2 * pi or diff < -pi and diff + 2 * pi or diff
		if diff <= 0 then
			factor = -1
		end
		i_lindworm.o = i_lindworm.o + factor * dt * 5
		i_lindworm.x = i_lindworm.x + dt * i_lindworm.s * math.cos(i_lindworm.o)
		i_lindworm.y = i_lindworm.y + dt * i_lindworm.s * math.sin(i_lindworm.o)
		
		--> Update segment movement!
		for i=1, #i_lindworm.segments, 1 do
			local v = i_lindworm.segments[i]
			if i == 1 then --> First segment, "head of lindworm", leads the way!
				v.x = i_lindworm.x
				v.y = i_lindworm.y
			else
				local dist = math.sqrt((i_lindworm.segments[i-1].x - v.x)^2 + (i_lindworm.segments[i-1].y - v.y)^2)
				if dist > v.radius * i_lindworm.segmentDistanceFactor then --> Move other segments as well, towards lower segment!
					local o = math.rad(math.atan2(i_lindworm.segments[i-1].y - v.y, i_lindworm.segments[i-1].x - v.x) * 180 / math.pi)
					v.x = v.x + dt * i_lindworm.s * math.cos(o)
					v.y = v.y + dt * i_lindworm.s * math.sin(o)
				end
			end
			--> Update particles!
			v.particleSystem:update(dt)
			--> Update colliders for each segment!
			v.collider:moveTo(v.x, v.y)
			
		end
		
		--> Update sound!
		i_lindworm.sound.source:setPosition(i_lindworm.x, i_lindworm.y)
		
	end,
		
	--> Creates and returns a new instance!
	create = function(parameters)
	
		local lindworm = {} --> New _lindworm!
		local parameters = parameters or {}
		
		--> General data!
		lindworm.className = "_lindworm" --> className.collide() is used for collisions!
		lindworm.type = "enemy" --> Used for e.g. collision groups!
		
		--> Position and speed parameters!
		lindworm.x = parameters.x or 0.5 * love.graphics.getWidth()
		lindworm.y = parameters.y or 0.5 * love.graphics.getHeight()
		lindworm.s = parameters.s or love.math.random(75, 125)
		lindworm.o = parameters.o or math.rad(love.math.random(0, 359))
				
		--> Colors and style!
		lindworm.r = parameters.r or _G.colours.danger.r or 255
		lindworm.g = parameters.g or _G.colours.danger.g or 0
		lindworm.b = parameters.b or _G.colours.danger.b or 0
		lindworm.a = parameters.a or 255
		lindworm.width = parameters.width or love.math.random(3, 5)
		
		--> Segments!
		lindworm.maxSeg = parameters.maxSeg or love.math.random(7, 20)
		lindworm.segments = parameters.segments or {} --> Holds all current segments {x, y}!
		lindworm.segRadius = parameters.segRadius or love.math.random(15, 20)
		lindworm.circleSegments = parameters.circleSegments or 125
		lindworm.segmentDistanceFactor = 1.5
		lindworm.smallestSegmentSizeInPercent = 50
		
		--> Target that the lindworm is moving to {x, y}!
		lindworm.target = nil
		lindworm.maxTargetRange = parameters.maxTargetRange or 300
		lindworm.minTargetRange = parameters.minTargetRange or 100
		lindworm.minTargetAngle = parameters.minTargetAngle or -75
		lindworm.maxTargetAngle = parameters.maxTargetAngle or 75
		lindworm.targetFoundRadius = 20
		
		--> Positioning limiters!
		lindworm.minX = parameters.minX or 0
		lindworm.maxX = parameters.maxX or 1600
		lindworm.minY = parameters.minY or 0
		lindworm.maxY = parameters.maxY or 900
		
		--> Particle System parameters and variables!
		lindworm.pImgX = 2
		lindworm.pImgY = 2
		lindworm.pAmount = 160
		lindworm.pImg = love.graphics.newImage((function() --> Particle images!
			local imageData = love.image.newImageData(lindworm.pImgX, lindworm.pImgY)
			for x=0, lindworm.pImgX-1, 1 do
				for y=0, lindworm.pImgY-1, 1 do
					imageData:setPixel(x, y, lindworm.r, lindworm.g, lindworm.b, lindworm.a)
				end
			end
			return imageData
		end)())
		
		--> Create all segments!
		local segSizeDrop = lindworm.smallestSegmentSizeInPercent/lindworm.maxSeg
		local size = 100
		local factor = 1
		if love.math.random(1, 2) == 1 then
			factor = -1
		end
		for i=1, lindworm.maxSeg, 1 do
			local segment = {x = lindworm.x, y = lindworm.y, radius = size * lindworm.segRadius / 100}
			size = size - segSizeDrop
			segment.particleSystem = love.graphics.newParticleSystem(lindworm.pImg, lindworm.pAmount)
			segment.particleSystem:setEmissionRate(32)
			segment.particleSystem:setEmitterLifetime(5)
			segment.particleSystem:setParticleLifetime(60*60*24*365, 60*60*24*365)
			segment.particleSystem:setLinearAcceleration(5, 5, 5, 5)
			segment.particleSystem:setRadialAcceleration(-100, -100)
			segment.particleSystem:setTangentialAcceleration(factor*1000, factor*1000)
			segment.particleSystem:setSpinVariation(0, 1)
			segment.particleSystem:setColors(lindworm.r, lindworm.g, lindworm.b, lindworm.a, lindworm.r, lindworm.g, lindworm.b, lindworm.a) --> Fade to black!
			segment.particleSystem:setLinearDamping(38 - segment.radius, 40 - segment.radius)
			
			--> Add a collider for each segment!
			segment.collider = _collider.HC:addCircle(segment.x, segment.y, segment.radius)
			_collider.HC:addToGroup(lindworm.type, segment.collider)
			segment.collider.backLink = lindworm
		
			table.insert(lindworm.segments, segment)
		end
		
		--> Add sound!
		lindworm.sound = _afx:play("enemyLindwormRattle.ogg", {loop = true, x = lindworm.x, y = lindworm.y, attenuation = {min = 0, max = 1000}})

		--> Add to instances and return!
		table.insert(_lindworm.instances, lindworm)	
		return lindworm
	end

}