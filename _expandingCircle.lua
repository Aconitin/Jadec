_expandingCircle = {

	instances = {},

	drawAll = function()
		for i=#_expandingCircle.instances, 1, -1 do
			local v = _expandingCircle.instances[i]
			_expandingCircle.draw(v)
		end
	end,
	
	draw = function(i_ec) --> The i_ec is instance of _expandingCircle!
		love.graphics.setColor(i_ec.r, i_ec.g, i_ec.b, i_ec.a)
		love.graphics.setLineWidth(i_ec.width)
		love.graphics.setLineStyle("smooth")
		love.graphics.setLineJoin("bevel")
		love.graphics.push()
		love.graphics.translate(i_ec.x, i_ec.y)
		love.graphics.rotate(i_ec.rotation)
		love.graphics.circle("line", 0, 0, i_ec.radius, i_ec.segments)
		love.graphics.pop()
	end,
	
	updateAll = function(dt)
		for i=#_expandingCircle.instances,1,-1 do
			local v = _expandingCircle.instances[i]
			_expandingCircle.update(v, dt)
			if v.removeable then
				table.remove(_expandingCircle.instances, i)
			end
		end
	end,
	
	update = function(i_ec, dt)
		i_ec.radius = i_ec.radius + i_ec.expandRate * dt
		i_ec.age = i_ec.age + dt
		if i_ec.age >= i_ec.lifetime then
			i_ec.a = i_ec.a - dt * i_ec.degradation
			if i_ec.a <= 0 then
				i_ec.removeable = true
			end
		end
	end,
	
	clear = function()
		for i=#_expandingCircle.instances, 1, -1 do
			_expandingCircle.instances[i].removeable = true
			table.remove(_expandingCircle.instances, i)
		end
	end,
	
	new = function(parameters)
		local ret = {}
		local p = parameters or {}

		ret.x = p.x or 0.5*love.graphics.getWidth()
		ret.y = p.y or 0.5*love.graphics.getHeight()
		ret.r = p.r or 255
		ret.g = p.g or 255
		ret.b = p.b or 255
		ret.a = p.a or 255
		ret.radius = p.radius or 0
		ret.expandRate = p.expandRate or 100
		ret.lifetime = p.lifetime or 3
		ret.degradation = p.degradation or 100 --> Alphavalue per second!
		ret.width = p.width or 2
		ret.segments = p.segments or 75
		ret.rotation = p.rotation or math.rad(0)
		
		ret.age = 0
		ret.removeable = false

		--> Add to instances!
		table.insert(_expandingCircle.instances, ret)
	end
}