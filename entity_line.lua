_line = {

	--[[ Info
	
		Uses _map, _collider	
	
	--]]

	--> The different types of steps/noises for echolocation that there are in the game!
	profiles = nil, --> Required in main!

	--> Creates a bunch of lines based on a given profile!
	createBunch = function(profileName, parameters)
	
		--> Assure profile exists!
		assert(_line.profiles[profileName], "Error: _line profile does not exist!")
	
		--> Make shortcut!
		local profile = _line.profiles[profileName]

		--> Copy profile into parameters!
		parameters.width = profile.width
		parameters.s = profile.speed
		parameters.a = profile.alpha
		parameters.lifetime = profile.lifetime
	
		--> Create a bunch of lines according to the given profile!
		for i=0, profile.nrOfLines-1, 1 do --> Make as many lines as specified in the profile!
			parameters.o = math.rad((0.5 + i) * (360/profile.nrOfLines))
			_line.create(parameters)
		end

	end,
	
	--> Creates a single line instance!
	create = function(parameters)
	
		--> Line scaffold!
		local line = {} --> New line instance!
		local mt = {
			__tostring = function(self)
				return "<_line instance>"
			end,
			}
		setmetatable(line, mt)
		local p = parameters or {}

		--> Basic line info!
		line.className = "_line" --> className.collide() is used for collisions!
		line.type = "neutral" --> Used for e.g. collision groups!
				
		--> Colorisation!
		line.r, line.g, line.b = _map.getCurrentTileRGB(p.x, p.y)
		line.a = p.a --> Taken from profile!
		
		--> Constants!
		line.style = "smooth"
		line.join = "bevel"
		
		--> Parameters!
		line.pos = {p.x, p.y, p.x, p.y} --> The (two) points that make up a line. Will get more points on e.g. a reflect!
		
		--> Line variables!
		line.o = p.o --> Direction to travel in!
		line.s = p.s
		line.width = p.width
		line.widthFactor = _map.getCurrentTileLineSizeFactor(p.x, p.y)
		line.xSign = p.xSign or 1 --> Factor for x movement!
		line.ySign = p.ySign or 1 --> Factor for y movement!
		line.age = p.age or 0
		line.currentTileType = _map.getTileAt(line.pos[1], line.pos[2])
		line.origin = p.origin or {} --> Where the line originally started from. If empty, it is pos 1 and 2!
		line.removeable = false
		line.degradation = p.degradation or line.a/p.lifetime
		line.resFactor = 0.0005
		line.frozen = false
		
		--> Collider!
		line.collider = _collider.HC:addPoint(line.pos[1], line.pos[2])
		_collider.HC:addToGroup(line.type, line.collider)
		line.collider.backLink = line
		_collider.HC:setPassive(line.collider)
	
		--> Add line to instances!
		table.insert(_line.instances, line)

	end,
	
	--> Holds line instances!
	instances = {},

	--> Draws all line instances!
	drawAll = function()
		love.graphics.setBlendMode("lighten", "premultiplied") --> Make sure transparency of line stays the same even on line overlaps!
		for i,v in pairs(_line.instances) do
			_line.draw(v)
		end
		love.graphics.setBlendMode("alpha", "alphamultiply")
	end,
	
	--> Draws a single line!
	draw = function(i_line)
		love.graphics.setLineStyle(i_line.style)
		love.graphics.setLineJoin(i_line.join)
		love.graphics.setColor(i_line.r*i_line.a/255, i_line.g*i_line.a/255, i_line.b*i_line.a/255, i_line.a) --> Premultiplied alpha blend mode!
		love.graphics.setLineWidth(i_line.width * i_line.widthFactor)
		love.graphics.line(i_line.pos)
	end,

	--> Updates all line instances!
	updateAll = function(dt)
		for i=#_line.instances, 1, -1 do
			local v = _line.instances[i]
			if v.removeable then
				_line.remove(v, i)
			else
				_line.update(v, dt)
			end
		end
	end,
	
	--> Updates one line instance!
	update = function(i_line, dt)
		i_line.age = i_line.age + dt
		i_line.a = i_line.a - dt*i_line.degradation
		if i_line.a < 0 then
			i_line.a = 0
			i_line.removeable = true
		end
		if not i_line.frozen then
			--> Update line color to squish bugs!
			local x, y = i_line.pos[#i_line.pos-1], i_line.pos[#i_line.pos]
			local dx = i_line.xSign*i_line.s*dt*math.cos(i_line.o)
			local dy = i_line.ySign*i_line.s*dt*math.sin(i_line.o)
			local blocked = false
			local newXSign, newYSign = i_line.xSign, i_line.ySign
			if not _map.isBlocked(x, y) then --> This is a workaround. There is a bug with the collision detection that occurs when a line hits an edge on-point. This just kills the line in such a case!
				if _map.isBlocked(x + dx, y) then
					blocked = true
					newXSign = -newXSign
				elseif _map.isBlocked(x, y + dy) then
					blocked = true
					newYSign = -newYSign
				end
				if blocked then
					local tdx, tdy = 0, 0
					local cddt = dt
					while true do
						if _map.isBlocked(x + tdx, y + tdy) or cddt <= 0 then
							break
						else
							cddt = cddt - i_line.resFactor
							tdx = tdx + i_line.xSign*i_line.s*math.cos(i_line.o) * i_line.resFactor
							tdy = tdy + i_line.ySign*i_line.s*math.sin(i_line.o) * i_line.resFactor
						end
					end
					i_line.pos[#i_line.pos-1] = i_line.pos[#i_line.pos-1] + tdx
					i_line.pos[#i_line.pos] = i_line.pos[#i_line.pos] + tdy
					i_line.xSign = newXSign
					i_line.ySign = newYSign
					x, y = i_line.pos[#i_line.pos-1], i_line.pos[#i_line.pos]
					table.insert(i_line.pos, x+i_line.xSign*i_line.s*i_line.resFactor*math.cos(i_line.o))
					table.insert(i_line.pos, y+i_line.ySign*i_line.s*i_line.resFactor*math.sin(i_line.o))
				else
					i_line.pos[#i_line.pos-1] = i_line.pos[#i_line.pos-1] + dx
					i_line.pos[#i_line.pos] = i_line.pos[#i_line.pos] + dy
				end
			end
			--> If tile type is changed (e.g. from water to air or whatever), freeze line and make new one with different color!
			local newTile = _map.getTileAt(i_line.pos[#i_line.pos-1], i_line.pos[#i_line.pos])
			if newTile and newTile ~= i_line.currentTileType then
				i_line.frozen = true
				_line.create({
						   x = i_line.pos[#i_line.pos-1],
						   y = i_line.pos[#i_line.pos],
						   o = i_line.o,
						   a = i_line.a,
						   s = i_line.s,
						   age = i_line.age,
						   degradation = i_line.degradation,
						   width = i_line.width,
						   xSign = i_line.xSign,
						   ySign = i_line.ySign,
						   origin = {x = i_line.pos[1], y = i_line.pos[2]},
				})
			end
		end
		if i_line.collider then
			i_line.collider:moveTo(i_line.pos[#i_line.pos-1], i_line.pos[#i_line.pos])
		end
	end,
	
	--> Removes one line!
	remove = function(i_line, ID)
		if i_line.collider then
			_collider.HC:remove(i_line.collider)
			i_line.collider = nil
		end
		table.remove(_line.instances, ID)
	end,
	
	--> Removes all lines!
	clear = function()
		for i=#_line.instances, 1, -1 do
			_line.remove(_line.instances[i], i)
		end
	end
	
}