_player = {

	--[[ Info
	
		drawAll()
		draw(i_player)
		updateAll(dt)
		update(i_player, dt)
		getMedianPosition()
		countAlivePlayers()
		collide(i_player, i_other)
		die(i_player)
		revive(i_player)
		resetAll()
		reset(i_player)
		setPosition(i_player, x, y)
		getNextFootstep(i_player)
		throw(i_player)
		step(i_player)
		sneak(i_player)
		run(i_player)
		clap(i_player)
		create(parameters)

	--]]
	
	--> Holds player instances!
	instances = {},
	
	--> Draws all players!
	drawAll = function()
		for i,v in ipairs(_player.instances) do
			_player.draw(v)
		end
	end,
	
	--> Draws a single player!
	draw = function(i_player)
	
		--> If player is throwing, draw line to aim!
		if i_player.isThrowing then
			i_player.dottedMovingLine:draw()
		end
	
		--> For debug draws only!
		if __drawColliders then
			love.graphics.setColor(255, 0, 0, 255)
			i_player.collider:draw()
			love.graphics.setColor(255, 255, 255, 255)
			love.graphics.circle("fill", i_player.x + 25*math.cos(i_player.r), i_player.y + 25*math.sin(i_player.r), 5, 25)
		end
		
	end,
	
	--> Updates all players!
	updateAll = function(dt)
		for i,v in ipairs(_player.instances) do
			_player.update(v, dt)
		end
	end,
	
	--> Updates a single player!
	update = function(i_player, dt)
	
	if not i_player.dead then
	
	
		
		--> First update movement!
		local moved = false
		local fullUpdate = i_player.controller:getFullUpdate(i_player.x, i_player.y) --> Get update from controller!
		local throwFactor = 1 --> This gets set to zero if the player must not move when he is about to throw a stone!
		local walkFactor = 0
		
		i_player.isThrowing = false
		if fullUpdate.isThrowing and i_player.readyToThrow then --> If the player is about to throw a stone, then do not allow it to move (just turn)!
			i_player.isThrowing = true
			throwFactor = 0
			i_player.dottedMovingLine:changeParameters(i_player.x + 25 * math.cos(i_player.r), i_player.y + 25 * math.sin(i_player.r), i_player.x + 500 * math.cos(i_player.r), i_player.y + 500 * math.sin(i_player.r), nil, nil, nil, false)
			i_player.dottedMovingLine:update(dt)
		end
		
		if fullUpdate.upDown ~= 0 or fullUpdate.leftRight ~= 0 then --> Move player accordingly (sneaking, running, etc)!
			walkFactor = 1
			moved = true
		end

		--> Determine parameters!
		local angleInDegrees = math.rad(math.atan2(fullUpdate.upDown, fullUpdate.leftRight) * 180 / math.pi) --> Calculate direction to walk in!
		local movementSpeed = i_player.s * (_map.tiles[_map.getTileAt(i_player.x, i_player.y)].movementSpeedFactor or 1) * throwFactor * walkFactor --> Get movement speed!
		local stepFrequency = 1
		local currentTile = _map.tiles[_map.getTileAt(i_player.x, i_player.y)].name
		
		if fullUpdate.isRunning and i_player.running == false and i_player.exhaustion <= 3 and moved then
			i_player.running = true --> Start to run!
		elseif (not fullUpdate.isRunning and i_player.running == true) or i_player.exhaustion >= 4 or moved == false then
			i_player.running = false --> End running!
		end

		
		
		
		
		i_player.exhaustion = i_player.exhaustion - 0.5 * dt
		if i_player.exhaustion <= 0 then
			i_player.exhaustion = 0
		end
		if i_player.running and currentTile == "_air" then
			movementSpeed = movementSpeed * 2
			stepFrequency = 1.5
			i_player.exhaustion = i_player.exhaustion + 1.5 * dt
		elseif fullUpdate.isSneaking then
			movementSpeed = movementSpeed * 0.5 --> Slow down player if he does sneak!
			stepFrequency = 0.75
		end
	
		
		if i_player.exhaustion > 0 and not i_player.running then
			i_player.breathCountdown = i_player.breathCountdown - dt
			if i_player.breathCountdown <= 0 then
				i_player.breathCountdown = 1
				
				if i_player.exhaustion >=0.5 then
				
					_afx:play("playerBreathShort.ogg")
					_line.createBunch("shortBreath", {x = i_player.x, y = i_player.y})

	
				
				else

					_afx:play("playerBreathLong.ogg")
					
					_line.createBunch("longBreath", {x = i_player.x, y = i_player.y})
					


				end
				
			end
		end
	
	
	
	
	
	
	
	
		--> Move player!
		if fullUpdate.leftRight ~= 0 then --> Player wants to move left or right!
			local dx = dt*movementSpeed*fullUpdate.leftRight
			local dpx = i_player.x + dx + 17*fullUpdate.leftRight
			local dp1y = i_player.y - 17
			local dp2y = i_player.y + 17
			if (not _map.isBlocked(dpx, dp1y)) and (not _map.isBlocked(dpx, dp2y)) then
				i_player.x = i_player.x + dx
			end
		end
		if fullUpdate.upDown ~= 0 then --> Player wants to move up or down!
			local dy = dt*movementSpeed*fullUpdate.upDown
			local dpy = i_player.y + dy + 17*fullUpdate.upDown
			local dp1x = i_player.x - 17
			local dp2x = i_player.x + 17
			if (not _map.isBlocked(dp1x, dpy)) and (not _map.isBlocked(dp2x, dpy)) then
				i_player.y = i_player.y + dy
			end
		end
		
--print(i_player.exhaustion, i_player.running)		
		i_player.idle = i_player.idle + dt
		 --> If he is standing still cause he is about to throw, don't make steps!
		if throwFactor == 1 and moved then
			i_player.idle = 0 --> Reset idle counter only if player isnt throwing!
			--> If he is sneaking, only draw the feet and don't play the sound!
			i_player.countTimeBetweenSteps = i_player.countTimeBetweenSteps + dt * stepFrequency
			if i_player.countTimeBetweenSteps >= i_player.timeBetweenSteps then
				i_player.countTimeBetweenSteps = 0
				if i_player.running == true and currentTile == "_air" then
					_player.run(i_player)
				elseif fullUpdate.isSneaking == true then
					_player.sneak(i_player)
				else
					_player.step(i_player)
				end
			end
		end
		
		
		--> Set new values!
		if fullUpdate.upDown ~= 0 or fullUpdate.leftRight ~= 0 then
			i_player.r = math.rad((math.atan2(fullUpdate.upDown, fullUpdate.leftRight) * 180 / math.pi))
		end
		i_player.collider:moveTo(i_player.x, i_player.y)

		--> If player does not move, draw both feet after a while of standing around!
		if i_player.idle >= i_player.maxIdle then
			i_player.idle = i_player.idle - i_player.maxIdle
		
			_footstep:new({x = i_player.x, y = i_player.y, r = i_player.r, lifetime = 7.5, left = true, right = true, leftImg = i_player.leftImg, rightImg = i_player.rightImg})
		end
		
		--> Update the type of block the player is standing on and call actions accordingly if the player just changed the block he is on!
		local ntt = _map.getTileAt(i_player.x, i_player.y) --> nbt = new tile type the player is standing on now!
		local ott = i_player.onBlockType --> ott = old tile type the player stood on before!
		if ntt ~= ott then --> Tile changed. (e.g. player walked into water from a normal _air tile)!
			if _map.tiles[ott] and _map.tiles[ott].offstepAction then
				_map.tiles[ott].offstepAction(i_player) --> Call action to happen when players leaves one tile!
			end
			i_player.onBlockType = _map.getTileAt(i_player.x, i_player.y)
			if _map.tiles[ntt] and _map.tiles[ntt].onstepAction then
				_map.tiles[ntt].onstepAction(i_player) --> Call action to happen when player steps on new tile!
			end
		end			
		
		
	end	
		
		
	end,
		
	--> Returns middle position of all players (for camera)!
	getMedianPosition = function()
		local x, y = 0, 0
		local count = 0
		for i,v in ipairs(_player.instances) do
			if not v.dead then
				count = count + 1
				x = x + v.x
				y = y + v.y
			end
		end
		if count ~= 0 then
			return x/count, y/count
		else
			return nil, nil
		end
	end,
	
	--> Returns number of alive players!
	countAlivePlayers = function()
		local c = 0
		for i,v in pairs(_player.instances) do
			if v.dead == false then
				c = c + 1
			end
		end
		return c
	end,
	
	--> Handles collision!
	collide = function(i_player, i_other)
		if i_other.className then
			if (i_other.className == "_lindworm") or (i_other.className == "_boomerang") or (i_other.className == "_whisp") then
				_player.die(i_player) --> Players dies when he touches an enemy!
			end
		end
	end,
	
	--> Lets a player die!
	die = function(i_player)
		i_player.dead = true
		i_player.controller.active = false
		_collider.HC:setGhost(i_player.collider)
		_afx:play("death.ogg")
		_footstep:new({x = i_player.x, y = i_player.y, r = i_player.r, lifetime = 10, left = true, right = true, cr = _G.colours.danger.r, cg = _G.colours.danger.g, cb = _G.colours.danger.b, leftImg = i_player.leftImg, rightImg = i_player.rightImg})
	end,

	--> Revives a player!
	revive = function(i_player)
		if i_player.dead then --> This check is done because at the start of each level all players are revived, even alive ones!
			i_player.dead = false
			i_player.controller.active = true
			_collider.HC:setSolid(i_player.collider)
			i_player.idle = i_player.maxIdle --> Makes footsteps appear instantly!
		end
	end,
	
	--> Resets all players!
	resetAll = function()
		for i,v in ipairs(_player.instances) do
			_player.reset(v)
		end
	end,
	
	--> Resets one player!
	reset = function(i_player)
		i_player.exhaustion = 0
		i_player.breathCountdown = 0
	end,
	
	--> Sets a players' position!
	setPosition = function(i_player, x, y)
		i_player.collider:moveTo(i_player.x, i_player.y)	
		i_player.x, i_player.y = x, y
	end,
	
	--> Returns wether the next footstep of a player will be a left or a right one!
	getNextFootstep = function(i_player)
		if i_player.lastStep == 1 then
			i_player.lastStep = 2
			return false, true
		elseif i_player.lastStep == 2 then
			i_player.lastStep = 1
			return true, false
		end
	end,

	--> Makes a normal footstep!
	step = function(i_player)
		local left, right = _player.getNextFootstep(i_player)
		local offset = -10
		if right then
			offset = 10
		end

		
		local x, y = i_player.x - offset * math.cos(math.deg(i_player.r) - 90), i_player.y - offset * math.sin(math.deg(i_player.r) - 90)
		_line.createBunch("step", {x = x, y = y})


		_footstep:new({x = i_player.x, y = i_player.y, r = i_player.r, lifetime = 10, left = left, right = right, leftImg = i_player.leftImg, rightImg = i_player.rightImg})
					
		--> Play sounds according to current tile or default to air tile!
		if not (left and right) then
			if left then
				_afx:play(_map.tiles[_map.getTileAt(i_player.x, i_player.y)].stepSoundLeft or _map.tiles["_air"].stepSoundLeft)
			elseif right then
				_afx:play(_map.tiles[_map.getTileAt(i_player.x, i_player.y)].stepSoundRight or _map.tiles["_air"].stepSoundRight)
			end
		end
		
	end,
		
	--> Makes a sneaky footstep!
	sneak = function(i_player)
		local left, right = _player.getNextFootstep(i_player)
		_footstep:new({x = i_player.x, y = i_player.y, r = i_player.r, lifetime = 10, left = left, right = right, leftImg = i_player.leftImg, rightImg = i_player.rightImg})
		
		--> Splash when in water!
		if _map.getTileAt(i_player.x, i_player.y) == "_water" then
			_afx:play("splashSneak.ogg")
			local offset = -10
			if right then
				offset = 10
			end
			
			local x, y = i_player.x - offset * math.cos(math.deg(i_player.r) - 90), i_player.y - offset * math.sin(math.deg(i_player.r) - 90)
			_line.createBunch("sneakInWater", {x = x, y = y})

			
		end
	end,
	
	--> Makes a running footstep!
	run = function(i_player)
		local left, right = _player.getNextFootstep(i_player)
		local offset = -10
		if right then
			offset = 10
		end
		
		local x = i_player.x - offset * math.cos(math.deg(i_player.r) - 90)
		local y = i_player.y - offset * math.sin(math.deg(i_player.r) - 90)
		_line.createBunch("run", {x = x, y = y})


		_footstep:new({x = i_player.x, y = i_player.y, r = i_player.r, lifetime = 10, left = left, right = right, leftImg = i_player.leftImg, rightImg = i_player.rightImg})
					
		--> Play sounds according to current tile or default to air tile!
		if not (left and right) then
			if left then
				_afx:play("runStepLeft.ogg")
			elseif right then
				_afx:play("runStepRight.ogg")
			end
		end
		
	end,
	
	--> Claps!
	clap = function(i_player)
		if i_player.readyToClap then
			i_player.readyToClap = false
			_hump.timer.after(0.5, function() i_player.readyToClap = true end)
			_afx:play("clap.ogg")
			_line.createBunch("clap", {x = i_player.x, y = i_player.y})
		end
	end,

	--> Throws a stone!
	throw = function(i_player)
		if i_player.readyToThrow then
			i_player.readyToThrow = false
			_hump.timer.after(0.5, function() i_player.readyToThrow = true end)
			_stone.create(i_player.x, i_player.y, i_player.r)
		end
	end,
	
	
	
	
	
	
	
	--> Creates new player instance!
	create = function(parameters)
		local ret = {}
		local mt = {
			__tostring = function(self)
				return "<_player instance>"
			end,
			}
		setmetatable(ret, mt)
		local p = parameters or {}

		ret.className = "_player"
		ret.type = "friendly" --> Used for e.g. collision groups!
		
		ret.controller = p.controller --> This is the controller this player will be using!
		ret.controller:unregisterCallback("join") --> Unregisters the "create player" callback of this controller!
		ret.x = p.x or _map.getMap().info.spawnpoint.x or 0
		ret.y = p.y or _map.getMap().info.spawnpoint.y or 0
		ret.s = p.s or 100 --> Speed of movement!
		ret.timeBetweenSteps = 0.5
		ret.countTimeBetweenSteps = 0
		ret.lastStep = 1 --> 0 = both, 1 = left, 2 = right!
		ret.r = p.r or math.rad(270)
		ret.idle = 2.0 --> How long the player has been not moving!
		ret.maxIdle = 2.0
		ret.dead = false
		ret.dottedMovingLine = _dottedMovingLine.create(0, 0, 0, 500, 150, 0.175, {255, 255, 255, 255}, true)
		

		
		ret.exhaustion = 0
		ret.breathCountdown = 0
		ret.running = false
		onBlockType = _map.getTileAt(ret.x, ret.y) --> Type of block/tile the player is on at the moment. Used to trigger changes!
		
		--> Cooldowns for clapping and throwing!
		ret.readyToClap = true
		ret.readyToThrow = true
		
		--> Collision detection variables!
		ret.collider = _collider.HC:addCircle(ret.x, ret.y, 18)
		_collider.HC:addToGroup(ret.type, ret.collider)
		ret.collider.backLink = ret
		_collider.HC:setPassive(ret.collider)

		--> Variables for footsteps!
		ret.leftImg = _footstep["leftImg0" .. (function() if #_player.instances%2 == 1 then return "2" end return "1" end)()] --> Decides which footprint to use by player "id"!
		ret.rightImg = _footstep["rightImg0" .. (function() if #_player.instances%2 == 1 then return "2" end return "1" end)()]

		--> Register some callbacks for this player!
		ret.controller:registerCallback(_player.throw, ret.controller.keyTable.throw, {ret}, "throw")
		ret.controller:registerCallback(_player.clap, ret.controller.keyTable.clap, {ret}, "clap")
		ret.controller:registerCallback(function(i_player) i_player.idle = i_player.maxIdle	end, ret.controller.keyTable.throwIdle, {ret}, "throwIdle")
		
		--> Add to instances!
		table.insert(_player.instances, ret)
		return ret
	end
}