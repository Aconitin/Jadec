_afx = { --> Sound effects and music!

	--> DebugPrint!
	debugPrint = function(c_afx, txt)
		--print(txt)
	end,

	--> Initializes class!
	init = function(c_afx, sourceFolders)
		c_afx:debugPrint("Initializing _afx class!")
		love.audio.setVolume(_G.configuration.masterVolume)
		love.audio.setDistanceModel("linear")
		--> Clean up!
		c_afx.sounds = nil
		c_afx.playing = nil
		c_afx.sourceFolders = nil
		c_afx.yard = nil
		c_afx.soundScenery = nil
		--> Create variables!
		c_afx.sourceFolders = sourceFolders
		c_afx.sounds = {} --> Contains loaded sound files!
		c_afx.playing = {} --> Contains sounds that are playing atm!
		c_afx.volumes = {sfx = _G.configuration.soundVolume, music = _G.configuration.musicVolume} --> Contains volumes indexed by sound source folder!
		c_afx.yard = {} --> Sources that stopped playing, for reuse, are put into here!
		c_afx.soundScenery = {} --> For making random background noises every some x amount of time!
		--> Load sounds!
		for i1,v1 in ipairs(c_afx.sourceFolders) do
			if not c_afx.volumes[v1] then
				c_afx.volumes[v1] = 1.0
				c_afx:debugPrint("Overwrote a volume!")
			end
			local files = love.filesystem.getDirectoryItems(v1)
			for i2, v2 in pairs(files) do
				local ret = {}
				ret["soundName"] = v2 --> Filename including extention!
				ret["soundPath"] = v1 .. "/" .. ret["soundName"] --> Relative soundFile path!
				ret["type"] = v1 --> Name of folder where the file is in (sfx, music)!
				c_afx.sounds[v2] = ret
				c_afx:debugPrint("Loaded soundfile " .. ret["soundPath"] .. " of type " .. ret["type"] .. "!")
			end
		end
		c_afx:debugPrint("_afx class initialized!")
	end,
		
	--> Plays a sound!
	play = function(c_afx, soundName, parameters)
		local parameters = parameters or {}
		local newSound = {}
		newSound = c_afx:revive(soundName) or {}
		newSound.soundName = soundName
		newSound.maxVolume = parameters.volume or c_afx.volumes[c_afx.sounds[soundName].type]
		if parameters.fade then --> For fading in (negative fade value)!
			newSound.volume = 0
			newSound.fade = newSound.maxVolume/parameters.fade
		else
			newSound.volume = newSound.maxVolume
		end
		newSound.soundPath = c_afx.sounds[soundName].soundPath
		if not newSound.source then
			--> Check if source is loaded already somewhere!
			local loaded = false
			for i,v in ipairs(c_afx.playing) do
				if v.soundName == soundName then
					newSound.source = v.source:clone()
					c_afx:debugPrint("Cloned new audio source!")
					loaded = true
					break
				end
			end
			if not loaded then
				for i,v in ipairs(c_afx.yard) do
					if v.soundName == soundName then
						newSound.source = v.source:clone()
						c_afx:debugPrint("Cloned new audio source!")
						loaded = true
						break
					end
				end
			end
			if not loaded then
				c_afx:debugPrint("Created new audio source!")
				newSound.source = love.audio.newSource(newSound.soundPath, "static")
			end
		end
		newSound.source:setVolume(newSound.volume)
		if parameters.x and parameters.y then
			newSound.source:setPosition(parameters.x, parameters.y, 0)
			local attenuation = parameters.attenuation or {min = 0, max = 2000}
			newSound.source:setAttenuationDistances(attenuation.min, attenuation.max)
		end
		if parameters.loop then
			newSound.source:setLooping(true)
		end
		love.audio.play(newSound.source)
		if parameters.seek then
			newSound.source:seek(parameters.seek)
		end
		table.insert(c_afx.playing, newSound)
		local seek = parameters.seek or "n/a"
		local fade = newSound.fade or parameters.fade or "nofade"
		c_afx:debugPrint("Playing sound " .. newSound.soundName .. " from " .. seek .. " with fade " .. fade .. "!")
		return newSound
	end,
	
	stop = function(c_afx, sound, parameters)
		local parameters = parameters or {}
		if parameters.fade then
			sound.fade = sound.volume/parameters.fade
		else
			if sound.source then sound.source:stop() end
		end
	end,
	
	--> Normal update function!
	update = function(c_afx, dt)
		for i=#c_afx.playing,1,-1 do
			local sound = c_afx.playing[i]
			if sound.fade then
				sound.volume = sound.volume - dt * sound.fade
				sound.source:setVolume(sound.volume)
			end
			if sound.volume <= 0 then
				sound.source:stop()
				sound.fade = nil
			end
			if sound.volume >= sound.maxVolume then
				sound.fade = nil
				sound.volume = sound.maxVolume
			end
			if sound.source:isStopped() then
				c_afx:debugPrint("Sound stopped playing: " .. sound.soundName)
				table.insert(c_afx.yard, table.remove(c_afx.playing, i)) --> Kill source if it has stopped playing!
			end
		end
		c_afx:updateSceneries(dt)
	end,
	
	--> For debug printing purposes only!
	getAllCounts = function(c_afx)
		local cs, cp, cv, csf, cy = 0, 0, 0, 0, 0
		for i,v in pairs(c_afx.sounds) do
			cs = cs + 1
		end
		for i,v in pairs(c_afx.playing) do
			cp = cp + 1
		end
		for i,v in pairs(c_afx.volumes) do
			cv = cv + 1
		end
		for i,v in pairs(c_afx.sourceFolders) do
			csf = csf + 1
		end
		for i,v in pairs(c_afx.yard) do
			cy = cy + 1
		end
		return cs, cp, cv, csf, cy
	end,
	
	--> Takes a used sound out of the yard!
	revive = function(c_afx, soundName)
		for i,v in pairs(c_afx.yard) do
			if v.soundName == soundName then
				return table.remove(c_afx.yard, i)
			end
		end
		return nil	
	end,
	
	--> Registers a new sound scenery!
	newScenery = function(c_afx, lowerTimeBound, upperTimeBound, possibleSounds, repeats, volume)
		local ret = {}
		ret.cd = love.math.random(lowerTimeBound, upperTimeBound)
		ret.lowerTimeBound = lowerTimeBound
		ret.upperTimeBound = upperTimeBound
		ret.possibleSounds = possibleSounds
		ret.volume = volume or _G.configuration.soundVolume
		ret.removeable = false
		ret.repeats = repeats --> Set this to a negative value for infinite repeats!
		table.insert(c_afx.soundScenery, ret)
	end,
	
	--> Updates all sound sceneries!
	updateSceneries = function(c_afx, dt)
		for i,v in ipairs(c_afx.soundScenery) do
			v.cd = v.cd - dt
			if v.cd <= 0 then
				v.cd = love.math.random(v.lowerTimeBound, v.upperTimeBound)
				v.repeats = v.repeats - 1
				if v.repeats == 0 then
					v.removeable = true
				end
				local randomSound = v.possibleSounds[love.math.random(1, #v.possibleSounds)]
				c_afx:play(randomSound, {volume = v.volume, x = love.math.random(1, love.graphics.getWidth()), y = love.math.random(1, love.graphics.getHeight())})
			end
		end
		c_afx:removeSceneries()
	end,
	
	--> Remove removeable sceneries!
	removeSceneries = function(c_afx)
		for i=#c_afx.soundScenery, 1, -1 do
			if c_afx.soundScenery[i].removeable then
				table.remove(c_afx.soundScenery, i)
			end
		end
	end,

}