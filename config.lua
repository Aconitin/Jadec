--###################################################################################--
--###################################################################################--

									-- ___                     
								   -- (   )                    
				 -- .-.    .---.    .-.| |    .--.     .--.    
				-- ( __)  / .-, \  /   \ |   /    \   /    \   
				-- (''") (__) ; | |  .-. |  |  .-. ; |  .-. ;  
				 -- | |    .'`  | | |  | |  |  | | | |  |(___) 
				 -- | |   / .'| | | |  | |  |  |/  | |  |      
				 -- | |  | /  | | | |  | |  |  ' _.' |  | ___  
				 -- | |  ; |  ; | | '  | |  |  .'.-. |  '(   ) 
			 -- ___ | |  ' `-'  | ' `-'  /  '  `-' / '  `-' |  
			-- (   )' |  `.__.'_.  `.__,'    `.__.'   `.__,'   
			 -- ; `-' '                                        
			  -- .__.'                                         
			  
--###################################################################################--
--								just a 'dark echo' clone							 --
--###################################################################################--

			  -- _                _                   _ _   _       
			 -- | |__  _   _     / \   ___ ___  _ __ (_) |_(_)_ __  
			 -- | '_ \| | | |   / _ \ / __/ _ \| '_ \| | __| | '_ \ 
			 -- | |_) | |_| |  / ___ \ (_| (_) | | | | | |_| | | | |
			 -- |_.__/ \__, | /_/   \_\___\___/|_| |_|_|\__|_|_| |_|
					-- |___/                                        

--###################################################################################--
--								     aconitin@gmx.de								 --
--###################################################################################--
-------------editor width: you must be at least this wide to enjoy the ride------------

	_G.configuration = {
	
							  -- __ _                       _   _             
			  -- ___ ___  _ __  / _(_) __ _ _   _ _ __ __ _| |_(_) ___  _ __  
			 -- / __/ _ \| '_ \| |_| |/ _` | | | | '__/ _` | __| |/ _ \| '_ \ 
			-- | (_| (_) | | | |  _| | (_| | |_| | | | (_| | |_| | (_) | | | |
			 -- \___\___/|_| |_|_| |_|\__, |\__,_|_|  \__,_|\__|_|\___/|_| |_|
								   -- |___/                                   

--###################################################################################--
--				     Feel free to modify the following variables!					 --
--###################################################################################--

	xResolution		=	1920		, --> Horizontal resolution of the game, in pixels!
	yResolution		=	1080			, --> Vertical resolution of the game, in pixels!
	fullscreen		=	true		, --> Toggles fullscreen mode (true or false)!
	fullscreentype	=	"desktop"	, --> Choose between "desktop" fullscreen or "exclusive" fullscreen mode!
	display			=	1			, --> Index of the monitor to show the game in!
	highdpi			=	false		, --> Enable high-dpi mode for the window on a Retina display!
	vsync			=	false		, --> Toggles vertical sync (true or false)!
	msaa			=	8			, --> Multi-sampled antialiasing (0, 2, 4, 8, 16, etc.)!
	masterVolume	=	0.99		, --> Master Volume (0.0 to 0.99)!
	soundVolume 	=	0.99		, --> Volume of ingame sound effects (0.0 to 0.99)!
	musicVolume 	=	0.6			, --> Volume of ingame music (0.0 to 0.99)!
	skipIntro		=	false		, --> Skip intro (true) or don't (false)!
	useFixedDT		=	true		, --> Toogles fixed dt (true/false). True is good!
	fixedDTStep		=	0.0166		, --> Step time for fixed dt!
	preBufferData	=	true		, --> Toggle preBuffering Data on game load to minimize ingame lags!

--###################################################################################--
--###################################################################################--

	} _G.keys = {

			 -- _                  
			-- | | _____ _   _ ___ 
			-- | |/ / _ \ | | / __|
			-- |   <  __/ |_| \__ \
			-- |_|\_\___|\__, |___/
					  -- |___/     

--###################################################################################--
--		  Here you can change the default keys for keyboard and controller!			 --
--###################################################################################--

	--> Controls for keyboard:
	keyboardUp			=	"w"			, --> 'Up' key on the keyboard!
	keyboardDown		=	"s"			, --> 'Down' key on the keyboard!
	keyboardLeft		=	"a"			, --> 'Left' key on the keyboard!
	keyboardRight		=	"d"			, --> 'Right' key on the keyboard!
	keyboardClap		=	"u"			, --> 'Clap' key on the keyboard!
	keyboardThrow		=	"i"			, --> 'Throw' key on the keyboard!
	keyboardSneak		=	"lshift"	, --> 'Sneak' key on the keyboard!
	keyboardRun			=	"lctrl"		, --> 'Run' key on the keyboard!
	keyboardJoin		=	"return"	, --> 'Join Game' key on the keyboard!
	
	--> Controls for gamepad:
	gamepadUpDown		=	2			, --> 'Left & Right' axis on the gamepad!
	gamepadLeftRight	=	1			, --> 'Up & Down' axis on the gamepad!
	gamepadSneak		=	3			, --> 'Sneak' axis on the gamepad!
	gamepadRun			=	6			, --> 'Run' axis on the gamepad!
	gamepadClap			=	"a"			, --> 'Clap' button on the gamepad!
	gamepadThrow		=	"b"			, --> 'Throw' button on the gamepad!
	gamepadJoin			=	"guide"		, --> 'Join Game' button on the gamepad!

--###################################################################################--
--###################################################################################--

	} _G.maps = {

			 -- _ __ ___   __ _ _ __  ___ 
			-- | '_ ` _ \ / _` | '_ \/ __|
			-- | | | | | | (_| | |_) \__ \
			-- |_| |_| |_|\__,_| .__/|___/
							-- |_|        

--###################################################################################--
--			 	This is for registering new, selfmade maps to the game!				 --
--###################################################################################--

	--> These maps are ordered and begin with the main menu. I wouldn't change this!
	--> If you want to begin at a later level, you can comment out levels!
	ordered = {
		[0] = "mainMenu"			, --> The main menu!
		[1] = "001 - lost"			, --> Lost (introduction to movement)!
		[2] = "002 - still lost"	, --> Still Lost (intro to movement II )!
		[3] = "003 - moisture"		, --> Moisture (intro to water)!
		[4] = "004 - the hole"		, --> The Hole (intro to holes)!
		[5] = "005 - crater"		, --> Crater (intro to holes II)!
		[6] = "006 - lindworms"		, --> Lindworms (intro to lindworms)! 
		[7] = "The Wormhole"		, --> Intro to Lindworms II
		[8] = "007 - whisps"		, --> Whisps (intro to whisps)!
		[9] = "008 - trapped"		, --> Trapped (intro to traps)!
		[10] = "Caved In",
		[11] = "Another Perfect Threat",
		[12] = "The Hallway",
	},
	
	--> These maps belong to a map pool. They are randomly chosen during the game!
	--> (You have to set the map to "true" to activate it!)
	unordered = {
		testMap = false,
	},
	
--###################################################################################--
--###################################################################################--

	} _G.colours = {

					   -- _                      
					  -- | |                     
			  -- ___ ___ | | ___  _   _ _ __ ___ 
			 -- / __/ _ \| |/ _ \| | | | '__/ __|
			-- | (_| (_) | | (_) | |_| | |  \__ \
			 -- \___\___/|_|\___/ \__,_|_|  |___/

--###################################################################################--
--		Colours used by the game. You can adjust those if you're colourblind!		 --
--###################################################################################--

	normal = {	r = 255,	--> The normal, white color!
				g = 255,
				b = 255},
	water = {	r = 0,		--> Color used for water (blue)!
				g = 0,
				b = 255},
	exit = {	r = 255,	--> Color used for the level exits (yellow)!
				g = 255,
				b = 0},
	danger = {	r = 255,	--> Color used for everything dangerous, like enemies!
				g = 0,
				b = 0},
	
--###################################################################################--
--###################################################################################--

	}