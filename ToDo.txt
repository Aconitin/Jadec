Whisp enemy class:
	Make the whisp not take targets that have the same origin twice (Maybe by list of origins or something)!
	Make the whisp abort movement when it is close to the goal, to make shaking of whisp stop!
	
Player class:
	Make countdown for throwing stones like for clapping
	Death animation

Tiles:
	!!Make some other tiles except walls and air, for example:
		mud
		lever
		key
		teleporters
	Improve tile generation algorithm to provide good levels!

Footsteps:
	Make the double footstep look better (Onto normal footsteps). Also make them not disappear and re-appear all the time?
	Re-align feet and line origin better!

Bugs:
	When FPS drop below 60, there are collision detection issues. Lines bug through walls, for example!
	Lines bug through tiles diagonal to each other
	Last breath does not always trigger
	Respawns

Workarounds:
	When a line hits an edge on-point, it weirds the fuck out. This is currently work-arounded by just freezing the line.

Map:
	make random map generation possible
	Make hunter be spawned for a new map based on some difficulty!

Until playable:
	enemy better --> level class
	
levels:
7	8	9	10	11	12	13	14	15	16	17	18	19	20

Other:
	!!!!! make use of metatables to index differently. local mt = { __index = _whisp }
	loading splash screen sucks
	weird line bug where on clap the one line is not as fast as the other lines
	raycast
	constant boomerang tempo
	schweif boomerang
	whisp makes click sound to hunt you down
	hump
	WEIRD CONFIG BUG THAT YOU CANT CLAP ANYMORE
	FASTER WHEN DIAGONAL
	player.resetall
	pitch whisp
	map generation threaded
	make bumping into walls a feature for possible hard mode
	main menu sucks at different resolutions
	make all attenuations!
	blade traps by isogash
	kill animations
	falls of ledges
	rhino
	line sizes!
	use bevel line join if fixed
	make footsteps colors different and work and not bug and good in mp and stuff
	game over
	add batman
	player death anim
	make new line class
	make lines light up walls
	replace all c_ variables with the actual classes!
	traps bugged when line goes though e.g. red first
	Make keys on gamepad be axis or buttons
	trigger sound when exit is first found
	Make better difficulty index
	kittens
	Make gamestates for death
	objective??
	circle enemy that gets bigger
	creeper enemy
	was was zeitdruck macht
	make this todo list better, it sucks.
	COOL ENEMY that is a plant that holds you for a second and squeaks
	not so squary level gen
	new monsters
	Exploding monsters that go boom when they hear too much noise Like static poles that glow up when hit by sound, and if too much sound hits it, it shoot projectiles like soundwaves
	Make sound for sneaking and all else
	Implement game over! Players that die will be respawned next dungeon. make it so that if all players are dead, its game over
	put shit start on start
	smooth movement
	psycho sounds
	maps suck ass
	loading screen sucks mega ass
	enemy that grows all the time
	creeper
	reset the player on gameClear (breath etc)
	make shit get place middleish
	color blind colors
	different death sounds
	reduce the collider size of boomerang and player?
	cooldown for stone
	titles
	death animation
	make whisps particle systems
	cool death animation
	subsubtitles
	make snake not weird around edges
	collider doesnt get moved bug
	remove at snake
	disable collider when player dies
	make colors in config
	think about the colors of the particle systems a bit more, esp alpha and while drawing
	make lastTarget for whisps so that they dont follow same targets or maybe lastage or whatnot
	make boomerang more scarey
	coll
	make all sounds fade when exiting a level
	different sounds for different deaths
