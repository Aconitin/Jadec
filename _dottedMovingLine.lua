_dottedMovingLine = {
	
	dotImg = love.graphics.newImage("gfx/dot.png", {mipmaps=true}),
	scale = 1/5,
	
	create = function(x1, y1, x2, y2, s, t, color, pregenerate)
	
		--> Parameters!
		local ret = {}
		ret.color = color or {255, 255, 255, 255}
		ret.x1 = x1 or 0
		ret.y1 = y1 or 0
		ret.x2 = x2 or 0
		ret.y2 = y2 or 0
		ret.s = s or 10
		ret.t = t or 1 --> Time between dots!
		ret.length = math.sqrt((x2 - x1)^2 + (y2 - y1)^2)
		ret.removeable = false

		--> Variables!
		ret.r = math.rad((math.atan2(ret.y2 - ret.y1, ret.x2 - ret.x1) * 180 / math.pi))
		ret.dt = t or 1
		ret.dots = {}
		
		--> Pre-generate some dots!
		if pregenerate then
			local distanceBetweenDots = ret.t * ret.s
			local i = 0
			while true do
				local radius = distanceBetweenDots * i
				if radius > ret.length then
					break
				end
				local a = 255 - ((radius/ret.length)*255)
				table.insert(ret.dots, _dottedMovingLine.dot(radius, ret.color[1], ret.color[2], ret.color[3], a))
				i = i + 1
			end
		end
		
		ret.changeParameters = function(i_dml, x1, y1, x2, y2, s, t, color, removeable)
			i_dml.color = color or i_dml.color
			i_dml.x1 = x1 or i_dml.x1
			i_dml.y1 = y1 or i_dml.y1
			i_dml.x2 = x2 or i_dml.x2
			i_dml.y2 = y2 or i_dml.y2
			i_dml.s = s or i_dml.s
			i_dml.t = t or i_dml.t
			i_dml.removeable = removeable or i_dml.removeable
			i_dml.r = math.rad((math.atan2(i_dml.y2 - i_dml.y1, i_dml.x2 - i_dml.x1) * 180 / math.pi))
			i_dml.length = math.sqrt((i_dml.x2 - i_dml.x1)^2 + (i_dml.y2 - i_dml.y1)^2)
		end
	
		ret.draw = function(i_dml)
			for i,v in pairs(i_dml.dots) do
				love.graphics.setColor(v.r, v.g, v.b, v.a)
				local x = i_dml.x1 + v.radius * math.cos(i_dml.r)
				local y = i_dml.y1 + v.radius * math.sin(i_dml.r)
				love.graphics.draw(_dottedMovingLine.dotImg, x, y, 0, _dottedMovingLine.scale, _dottedMovingLine.scale, 0.5 * _dottedMovingLine.dotImg:getWidth(), 0.5 * _dottedMovingLine.dotImg:getHeight())
			end
		end
		
		ret.update = function(i_dml, dt)
			i_dml.dt = i_dml.dt - dt
			if i_dml.dt <= 0 then
				i_dml.dt = i_dml.t
				table.insert(i_dml.dots, _dottedMovingLine.dot(0, i_dml.color[1], i_dml.color[2], i_dml.color[3], i_dml.color[4]))
			end
			
			--> Update dots!
			for i=#i_dml.dots,1,-1 do
				local v = i_dml.dots[i]
				v.radius = v.radius + dt * i_dml.s
				v.a = 255 - ((v.radius/i_dml.length)*255)
				if v.a <= 0 then
					table.remove(i_dml.dots, i)
				end
			end
		end
	
		--> Push to instances!
		return ret
	end,
	
	dot = function(radius, r, g, b, a)
		local ret = {}
		ret.radius = radius or 0
		ret.r = r or 255
		ret.g = g or 255
		ret.b = b or 255
		ret.a = a or 255
		return ret
	end,
}