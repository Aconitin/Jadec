--[[ This section of code was written by @Isogash and is licensed under the following license:

							DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
										Version 2, December 2004

							 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

							 Everyone is permitted to copy and distribute verbatim or modified
							 copies of this license document, and changing it is allowed as long
							 as the name is changed.

										DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
							TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

									0. You just DO WHAT THE FUCK YOU WANT TO.

 --]]

local line = love.graphics.line

local function normalize(x, y)
	local len = math.sqrt((x ^ 2) + (y ^ 2))
	return (x / len), (y / len)
end

local function dot(x1,y1,x2,y2)
	return x1*x2 + y1*y2
end

local function reflected(x1,y1,x2,y2,x3,y3)
	x1, y1 = normalize(x2-x1,y2-y1)
	x2, y2 = normalize(x2-x3,y2-y3)
	return dot(x1,y1,x2,y2) > 0.95
end

function love.graphics.line(...)
	local a = ...
	local points
	if type(a) == "table" then
		points = a
	else
		points = {}
		for i,v in ipairs({...}) do
			table.insert(points, v)
		end
	end

	local joinpoints = {}
	local i = 1
	while i < table.getn(points) do
		if joinpoints[3] and reflected(points[i-4], points[i-3], points[i-2], points[i-1], points[i], points[i+1]) then
			line(joinpoints)
			joinpoints = {}
			table.insert(joinpoints, points[i-2])
			table.insert(joinpoints, points[i-1])
		end
		table.insert(joinpoints, points[i])
		table.insert(joinpoints, points[i+1])
		i = i + 2
	end
	line(joinpoints)
end