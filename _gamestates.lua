_gamestates = {

	--> Class variables!
	current = "nil", --> Current Gamestate name!
	
	--> Changes gamestate!
	switchTo = function(name)
		if _gamestates[_gamestates.current] and _gamestates[_gamestates.current].after then
			_gamestates[_gamestates.current].after()
		end
		_gamestates.current = name
		if _gamestates[_gamestates.current].before then
			_gamestates[_gamestates.current].before()
		end
	end,
	
	--> Updates the games!
	updateGame = function(dt)
		
		--> Set camera!
		local ncpx, ncpy = _player.getMedianPosition() --> New camera positions!
		if ncpx and ncpy then
			_gamestates[_gamestates.current].camera:setPosition(ncpx, ncpy)
			love.audio.setPosition(ncpx, ncpy, 0) --> And audio!
		end

		--> Update enemies!
		_whisp.updateAll(dt)
		_boomerangTrap.updateAll(dt) --> This also updates all boomerangs!
		_lindworm.updateAll(dt)
		
		--> Update rest!
		_stone.updateAll(dt)
		_expandingCircle.updateAll(dt)
		_footstep:updateAll(dt)
		_afx:update(dt)
		_line.updateAll(dt)
		_map.update(dt)
		_player.updateAll(dt)
		
		--> Update collision detection!
		_collider.HC:update(dt)
		
		--> Update hump libraries!
		_hump.timer.update(dt)
		
	end,
	
	--> Draws the game!
	drawGame = function()
		_map.draw()
		_line.drawAll()
		_footstep:drawAll()
		_player.drawAll() --> Only for throw line drawings!
		_stone.drawAll() --> Only for debug drawings!
		_expandingCircle.drawAll()
		_boomerangTrap.drawAll()
		_lindworm.drawAll()
		_whisp.drawAll()
	end,
	
	--> Clears the game, removes all entities and effects and resets the players. To be called before e.g. loading a new map!
	clearGame = function()
		_player.resetAll()
		_line.clear()
		_footstep.clear()
		_whisp.clear()
		_expandingCircle.clear()
		_map.clearText()
		_boomerangTrap.clear()
		_lindworm.clear()
		_stone.instances = {}
		_hump.timer.clear()
	end,
		
	--> Registered gamestates!
	splash = nil, --> Required in main!
	intro = nil, --> Required in main!
	leaving = nil, --> Required in main!
	menu = nil, --> Required in main!
	ingame = nil, --> Required in main!
	
}